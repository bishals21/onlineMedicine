import 'dart:async';
import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/foundation.dart';
import 'package:online_medicine/model/onlineMedicineItem.dart';
import 'package:online_medicine/model/userAccessToken.dart';
import 'package:shared_preferences/shared_preferences.dart';

class StateContainer extends StatefulWidget {
  final Widget child;
  final List<OnlineMedicineItem> cartItemList;
  final List<OnlineMedicineItem> medicineList;
  final List<OnlineMedicineItem> substituteList;
  final List<OnlineMedicineItem> specialOfferList;

  StateContainer(
      {@required this.child,
      this.cartItemList,
      this.medicineList,
      this.substituteList,
      this.specialOfferList});

  static StateContainerState of(BuildContext context) {
    return (context.inheritFromWidgetOfExactType(_InheritedStateContainer)
            as _InheritedStateContainer)
        .data;
  }

  @override
  StateContainerState createState() => new StateContainerState();
}

class StateContainerState extends State<StateContainer> {
  Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
  List<OnlineMedicineItem> cartItemList = new List();
  List<OnlineMedicineItem> medicineList = new List();
  List<OnlineMedicineItem> substituteList = new List();
  List<OnlineMedicineItem> specialOfferList = new List();

  UserAccessToken userAccessToken;

  void updateCartList(List<OnlineMedicineItem> cartItemLists) {
    setState(() {
      cartItemList = cartItemLists;
      _changeCartList(cartItemLists);
    });
  }

  void updateMedicineList(
      List<OnlineMedicineItem> medicineLists, bool refresh) {
    if (refresh) {
      setState(() {
        medicineList = medicineLists;
      });
    } else {
      medicineList = medicineLists;
    }
  }

  void updateSubstituteList(
      List<OnlineMedicineItem> substituteLists, bool refresh) {
    if (refresh) {
      setState(() {
        substituteList = substituteLists;
      });
    } else {
      substituteList = substituteLists;
    }
  }

  void updateSpecialOfferList(
      List<OnlineMedicineItem> specialOfferLists, bool refresh) {
    if (refresh) {
      setState(() {
        specialOfferList = specialOfferLists;
      });
    } else {
      specialOfferList = specialOfferLists;
    }
  }

  void updateUserAccessToken(UserAccessToken accessToken) {
    setState(() {
      this.userAccessToken = accessToken;
      _changeUserAccessToken(userAccessToken);
    });
  }

  _changeCartList(List<OnlineMedicineItem> cartItemLists) async {
    final SharedPreferences prefs = await _prefs;

    bool isSuccess =
        await prefs.setString('OMcartList', json.encode(cartItemLists));

    setState(() {
      if (isSuccess) {
        print("Success");
      }
    });
  }

  _changeUserAccessToken(UserAccessToken userAccessToken) async {
    final SharedPreferences prefs = await _prefs;
    bool isSuccess = false;
    if (userAccessToken != null) {
      isSuccess = await prefs.setString('OMUser', json.encode(userAccessToken));
    } else {
      await prefs.setString('OMUser', "null");
    }

    setState(() {
      if (isSuccess) {
        print("Success");
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return new _InheritedStateContainer(
      data: this,
      child: widget.child,
    );
  }
}

class _InheritedStateContainer extends InheritedWidget {
  final StateContainerState data;

  _InheritedStateContainer({
    Key key,
    @required this.data,
    @required Widget child,
  }) : super(key: key, child: child);

  @override
  bool updateShouldNotify(_InheritedStateContainer old) {
    return true;
  }
//   @override
//   bool updateShouldNotify(_InheritedStateContainer old) => true;
}
