import 'package:flutter/material.dart';

class HeroPhotoViewWrapper extends StatelessWidget {
  final String file;
  final String tag;

  const HeroPhotoViewWrapper({this.file, this.tag});

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      backgroundColor: Colors.black,
      appBar: new AppBar(
        backgroundColor: Colors.black,
      ),
      body: new Hero(
        tag: tag,
        child: new Container(
          decoration: new BoxDecoration(
              image: new DecorationImage(
            image: new NetworkImage(file),
            fit: BoxFit.cover,
          )),
        ),
      ),
    );
  }
}
