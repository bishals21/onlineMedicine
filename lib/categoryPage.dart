import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:online_medicine/appText.dart';
import 'package:online_medicine/appbar/gradientWithCart.dart';
import 'package:online_medicine/medicineDetailPage.dart';
import 'package:online_medicine/model/onlineMedicineItem.dart';
import 'package:online_medicine/stateContainer/stateContainer.dart';

class CategoryPage extends StatefulWidget {
  final id, title;
  CategoryPage({Key key, @required this.id, @required this.title})
      : super(key: key);
  @override
  _CategoryPageState createState() => _CategoryPageState();
}

class _CategoryPageState extends State<CategoryPage> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

//   List<OnlineMedicineItem> list = new List();
  int currentPage = 1;
  bool hasMore = false;
  String query = "";

  Future requestLoadMore;
  bool isEmptyCheck = false;

  void _showSnackBar(String text) {
    _scaffoldKey.currentState.showSnackBar(new SnackBar(
      content: new Text(text),
    ));
  }

  Future<Null> _handleRefresh() async {
    print('_handleRefresh');
    final Completer<Null> completer = new Completer<Null>();
    List<OnlineMedicineItem> listOfItem = new List();
    final container = StateContainer.of(context);
    container.updateMedicineList([], true);
    bool isConnected = await checkConnection();
    if (isConnected) {
      http.get(
          AppText.baseURl +
              "medicines?_format=json&page=" +
              currentPage.toString() +
              "&category=" +
              widget.id.toString() +
              "&text=" +
              query,
          headers: {
            "api-key": AppText.apiKey,
          }).then((response) {
        if (response.statusCode == 200) {
          var responseBody = json.decode(response.body);
          var data = responseBody["data"];
          var medicines = responseBody["data"]["medicines"];

          print(medicines);
          for (var item in medicines) {
            listOfItem.add(new OnlineMedicineItem(
              item["id"],
              item["name"],
              item["image"],
              item["description"],
              item["company"],
              item["companyId"],
              item["category"],
              item["categoryId"].toString(),
              item["composition"],
              item["rate"].toString(),
              item["offer"].toString(),
              item["applicableRate"].toString(),
              item["rateDescription"],
              item["isPrescriptionRequired"],
              0,
            ));
          }

          for (int i = 0; i < container.cartItemList.length; i++) {
            for (int j = 0; j < listOfItem.length; j++) {
              if (container.cartItemList[i].id == listOfItem[j].id) {
                OnlineMedicineItem item = listOfItem[j];
                item.updateAddToCart(container.cartItemList[i].quantityInCart);
                listOfItem.removeAt(j);
                listOfItem.insert(j, item);
              }
            }
          }

          if (mounted) {
            currentPage = data["currentPage"];
            print(currentPage);
            print(data["nextPage"].toString());
            if (data["nextPage"] is String) {
              hasMore = false;
            } else {
              hasMore = true;
            }

            isEmptyCheck = true;
            print(hasMore);

            completer.complete(null);
            container.updateMedicineList(listOfItem, true);
          }

          return completer.future;
        }
      });
    } else {
      _showSnackBar("No Internet Connection");
      return completer.future;
    }
  }

  Future<Null> _handleLoadMore() async {
    bool isConnected = await checkConnection();
    final container = StateContainer.of(context);
    List<OnlineMedicineItem> containerList = container.medicineList;
    final Completer<Null> completer = new Completer<Null>();
    if (isConnected) {
      containerList.add(null);
      container.updateMedicineList(containerList, true);
      int newPage = currentPage + 1;

      var nextPageUrl = AppText.baseURl +
          "medicines?_format=json&page=" +
          newPage.toString() +
          "&category=" +
          widget.id.toString() +
          "&text=" +
          query;
      print('_handleLoadMore' + nextPageUrl);
      List<OnlineMedicineItem> listOfItem = new List();

      http.get(nextPageUrl, headers: {
        "api-key": AppText.apiKey,
      }).then((response) {
        if (response.statusCode == 200) {
          var responseBody = json.decode(response.body);
          var data = responseBody["data"];
          var medicines = responseBody["data"]["medicines"];

          print(medicines);
          for (var item in medicines) {
            listOfItem.add(new OnlineMedicineItem(
              item["id"],
              item["name"],
              item["image"],
              item["description"],
              item["company"],
              item["companyId"],
              item["category"],
              item["categoryId"].toString(),
              item["composition"],
              item["rate"].toString(),
              item["offer"].toString(),
              item["applicableRate"].toString(),
              item["rateDescription"],
              item["isPrescriptionRequired"],
              0,
            ));
          }

          for (int i = 0; i < container.cartItemList.length; i++) {
            for (int j = 0; j < listOfItem.length; j++) {
              if (container.cartItemList[i].id == listOfItem[j].id) {
                OnlineMedicineItem item = listOfItem[j];
                item.updateAddToCart(container.cartItemList[i].quantityInCart);
                listOfItem.removeAt(j);
                listOfItem.insert(j, item);
              }
            }
          }

          if (mounted) {
            containerList.removeLast();
            containerList.addAll(listOfItem);
            currentPage = data["currentPage"];
            print(currentPage);
            print(data["nextPage"].toString());
            if (data["nextPage"] is String) {
              hasMore = false;
            } else {
              hasMore = true;
            }
            print(hasMore);

            completer.complete(null);
            container.updateMedicineList(containerList, true);
          }
        }
      });
    } else {
      _showSnackBar("No Internet Connection");
      return completer.future;
    }

    return completer.future;
  }

  lockedLoadNext() {
    if (this.requestLoadMore == null) {
      this.requestLoadMore = _handleLoadMore().then((x) {
        this.requestLoadMore = null;
      });
    }
  }

  Future<bool> checkConnection() async {
    bool connected = false;
    try {
      final result = await InternetAddress.lookup('google.com');
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        print('connected');
        connected = true;
      }
    } on SocketException catch (_) {
      print('not connected');
      connected = false;
    }

    return connected;
  }

  @override
  void initState() {
    new Future.delayed(Duration.zero, () {
      _handleRefresh();
    });

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final container = StateContainer.of(context);
    List<OnlineMedicineItem> cartList = container.cartItemList;
    List<OnlineMedicineItem> list = container.medicineList;

    addToCart(OnlineMedicineItem item, int index) {
      OnlineMedicineItem addedItem = item;
      addedItem.quantityInCart = 1;
      list.removeAt(index);
      list.insert(index, addedItem);
      container.updateMedicineList(list, false);
      cartList.add(item);
      container.updateCartList(cartList);
    }

    updateCartItem(OnlineMedicineItem item, int index, bool method) {
      OnlineMedicineItem updateItem = item;
      int quantity = updateItem.quantityInCart;
      if (method == true) {
        updateItem.quantityInCart = quantity + 1;
        list.removeAt(index);
        list.insert(index, updateItem);
        container.updateMedicineList(list, false);
        cartList.forEach((item) {
          if (item.id == updateItem.id) {
            cartList.remove(item);
            cartList.add(updateItem);
          }
        });
        container.updateCartList(cartList);
      } else {
        if (quantity == 1) {
          updateItem.quantityInCart = 0;
          list.removeAt(index);
          list.insert(index, updateItem);
          container.updateMedicineList(list, false);
          cartList.removeWhere((item) => item.id == updateItem.id);
          container.updateCartList(cartList);
        } else {
          updateItem.quantityInCart = quantity - 1;
          list.removeAt(index);
          list.insert(index, updateItem);
          container.updateMedicineList(list, false);
          cartList.forEach((item) {
            if (item.id == updateItem.id) {
              cartList.remove(item);
              cartList.add(updateItem);
            }
          });
          container.updateCartList(cartList);
        }
      }
    }

    ThemeData buildTheme() {
      final ThemeData base = ThemeData();
      return base.copyWith(
        hintColor: Colors.grey,
        primaryColor: const Color(0xFF262BA0),
      );
    }

    return new Theme(
      data: buildTheme(),
      child: new Scaffold(
        key: _scaffoldKey,
        //backgroundColor: const Color(0xFFDCDCDC),
        body: new Column(
          children: <Widget>[
            new GradientAppBarWithCart(widget.title, cartList, false),
            new Padding(
              padding: const EdgeInsets.all(8.0),
              child: new TextField(
                style: new TextStyle(
                  fontSize: 16.0,
                  color: Colors.black,
                ),
                decoration: new InputDecoration(
                  contentPadding: new EdgeInsets.only(
                      top: 16.0, bottom: 16.0, left: 12.0, right: 12.0),
                  border: new OutlineInputBorder(),
                  prefixIcon: new Icon(Icons.search),
                  hintText: "Search",
                ),
                onChanged: (value) {
                  query = value;
                  _handleRefresh();
                  isEmptyCheck = false;
                },
              ),
            ),
            new Expanded(
              child: new CustomScrollView(
                slivers: <Widget>[
                  list.isNotEmpty
                      ? new SliverPadding(
                          padding: new EdgeInsets.all(8.0),
                          sliver: new SliverFixedExtentList(
                            itemExtent: 145.0,
                            delegate: new SliverChildBuilderDelegate(
                              (context, index) {
                                if (index + 2 > list.length) {
                                  if (hasMore) {
                                    lockedLoadNext();
                                  }
                                }

                                return list[index] != null
                                    ? new InkWell(
                                        onTap: () {
                                          //   Navigator.of(context).push(
                                          //     new PageRouteBuilder(
                                          //       pageBuilder: (context, animation,
                                          //               secondaryAnimation) =>
                                          //           new MedicineDetailPage(
                                          //             onlineMedicineItem:
                                          //                 list[index],
                                          //           ),
                                          //       transitionsBuilder: (context,
                                          //           animation,
                                          //           secondaryAnimation,
                                          //           child) {
                                          //         return new SlideTransition(
                                          //           position: new Tween<Offset>(
                                          //             begin:
                                          //                 const Offset(0.0, 1.0),
                                          //             end: Offset.zero,
                                          //           ).animate(animation),
                                          //           child: new SlideTransition(
                                          //             position: new Tween<Offset>(
                                          //               begin: Offset.zero,
                                          //               end: const Offset(
                                          //                   0.0, 1.0),
                                          //             ).animate(
                                          //                 secondaryAnimation),
                                          //             child: child,
                                          //           ),
                                          //         );
                                          //       },
                                          //       transitionDuration:
                                          //           const Duration(
                                          //               milliseconds: 500),
                                          //     ),
                                          //   );
                                          Navigator.of(context).push(
                                              MedicineDetailRoute(list[index]));
                                        },
                                        child: new Container(
                                          decoration: new BoxDecoration(
                                            border: new Border(
                                              bottom: new BorderSide(
                                                width: 0.5,
                                              ),
                                            ),
                                          ),
                                          padding: new EdgeInsets.all(8.0),
                                          child: new Column(
                                            mainAxisAlignment:
                                                MainAxisAlignment.center,
                                            children: <Widget>[
                                              new Row(
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                children: <Widget>[
                                                  new Container(
                                                    height: 90.0,
                                                    width: 90.0,
                                                    decoration:
                                                        new BoxDecoration(
                                                      border: new Border.all(
                                                        color: Colors.grey,
                                                        width: 0.7,
                                                      ),
                                                      borderRadius: BorderRadius
                                                          .all(new Radius
                                                              .circular(8.0)),
                                                    ),
                                                    child: new Hero(
                                                      tag: "image" +
                                                          list[index]
                                                              .id
                                                              .toString(),
                                                      child: new Center(
                                                        child: list[index]
                                                                    .image !=
                                                                null
                                                            ? new Image.network(
                                                                AppText.prefixImage +
                                                                    list[index]
                                                                        .image,
                                                                height: 65.0,
                                                                width: 65.0,
                                                              )
                                                            : new Image.asset(
                                                                "assets/medication.png",
                                                                height: 65.0,
                                                                width: 65.0,
                                                              ),
                                                      ),
                                                    ),
                                                  ),
                                                  new Flexible(
                                                    //padding: const EdgeInsets.all(8.0),
                                                    child: Padding(
                                                      padding:
                                                          const EdgeInsets.all(
                                                              8.0),
                                                      child: new Column(
                                                        crossAxisAlignment:
                                                            CrossAxisAlignment
                                                                .start,
                                                        children: <Widget>[
                                                          new Hero(
                                                            tag: "name" +
                                                                list[index]
                                                                    .id
                                                                    .toString(),
                                                            child: new Material(
                                                              color: Colors
                                                                  .transparent,
                                                              child: new Text(
                                                                list[index]
                                                                    .name,
                                                                maxLines: 1,
                                                                overflow:
                                                                    TextOverflow
                                                                        .ellipsis,
                                                                style:
                                                                    new TextStyle(
                                                                  fontSize:
                                                                      16.0,
                                                                  fontWeight:
                                                                      FontWeight
                                                                          .bold,
                                                                ),
                                                              ),
                                                            ),
                                                          ),
                                                          new Padding(
                                                            padding:
                                                                new EdgeInsets
                                                                        .only(
                                                                    top: 5.0),
                                                          ),
                                                          new Hero(
                                                            tag: "company" +
                                                                list[index]
                                                                    .id
                                                                    .toString(),
                                                            child: new Material(
                                                              color: Colors
                                                                  .transparent,
                                                              child: new Text(
                                                                list[index]
                                                                    .company,
                                                                maxLines: 1,
                                                                overflow:
                                                                    TextOverflow
                                                                        .ellipsis,
                                                                style:
                                                                    new TextStyle(
                                                                  fontSize:
                                                                      15.0,
                                                                ),
                                                              ),
                                                            ),
                                                          ),
                                                          new Padding(
                                                            padding:
                                                                new EdgeInsets
                                                                        .only(
                                                                    top: 5.0),
                                                          ),
                                                          list[index].rate ==
                                                                  list[index]
                                                                      .applicableRate
                                                              ? new Hero(
                                                                  tag: "applicableRate" +
                                                                      "${list[index].id}",
                                                                  child:
                                                                      new Material(
                                                                    color: Colors
                                                                        .transparent,
                                                                    child:
                                                                        new Text(
                                                                      "Rs. " +
                                                                          list[index]
                                                                              .applicableRate,
                                                                      style:
                                                                          new TextStyle(
                                                                        fontWeight:
                                                                            FontWeight.bold,
                                                                      ),
                                                                    ),
                                                                  ),
                                                                )
                                                              : new Row(
                                                                  children: <
                                                                      Widget>[
                                                                    new Hero(
                                                                      tag: "rate" +
                                                                          "${list[index].id}",
                                                                      child:
                                                                          new Material(
                                                                        color: Colors
                                                                            .transparent,
                                                                        child:
                                                                            new Text(
                                                                          "Rs. " +
                                                                              list[index].rate,
                                                                          style:
                                                                              new TextStyle(
                                                                            fontWeight:
                                                                                FontWeight.bold,
                                                                            color:
                                                                                Colors.red,
                                                                            decoration:
                                                                                TextDecoration.lineThrough,
                                                                          ),
                                                                        ),
                                                                      ),
                                                                    ),
                                                                    new Padding(
                                                                      padding: new EdgeInsets
                                                                              .only(
                                                                          left:
                                                                              16.0),
                                                                    ),
                                                                    new Hero(
                                                                      tag: "applicableRate" +
                                                                          "${list[index].id}",
                                                                      child:
                                                                          new Material(
                                                                        color: Colors
                                                                            .transparent,
                                                                        child:
                                                                            new Text(
                                                                          "Rs. " +
                                                                              list[index].applicableRate,
                                                                          style:
                                                                              new TextStyle(
                                                                            fontWeight:
                                                                                FontWeight.bold,
                                                                          ),
                                                                        ),
                                                                      ),
                                                                    ),
                                                                  ],
                                                                ),
                                                        ],
                                                      ),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                              new Align(
                                                alignment:
                                                    Alignment.bottomRight,
                                                child: list[index]
                                                            .quantityInCart ==
                                                        0
                                                    ? new InkWell(
                                                        splashColor:
                                                            Colors.black,
                                                        //onTap: calculateBMI,
                                                        onTap: () {
                                                          addToCart(list[index],
                                                              index);
                                                        },
                                                        child: new Container(
                                                          height: 35.0,
                                                          width: 110.0,
                                                          decoration:
                                                              new BoxDecoration(
                                                            gradient: new LinearGradient(
                                                                colors: [
                                                                  const Color(
                                                                      0xF29E30BF),
                                                                  const Color(
                                                                      0xF2262BA0)
                                                                ],
                                                                begin:
                                                                    const FractionalOffset(
                                                                        0.0,
                                                                        0.0),
                                                                end:
                                                                    const FractionalOffset(
                                                                        1.0,
                                                                        0.0),
                                                                stops: [
                                                                  0.0,
                                                                  1.0
                                                                ],
                                                                tileMode:
                                                                    TileMode
                                                                        .clamp),
                                                          ),
                                                          child: new Center(
                                                            child: new Text(
                                                              "ADD TO CART",
                                                              style:
                                                                  new TextStyle(
                                                                color: Colors
                                                                    .white,
                                                                fontWeight:
                                                                    FontWeight
                                                                        .w600,
                                                              ),
                                                            ),
                                                          ),
                                                        ),
                                                      )
                                                    : new Container(
                                                        width: 110.0,
                                                        child: new Row(
                                                          mainAxisAlignment:
                                                              MainAxisAlignment
                                                                  .spaceBetween,
                                                          children: <Widget>[
                                                            new GestureDetector(
                                                              onTap: () {
                                                                updateCartItem(
                                                                    list[index],
                                                                    index,
                                                                    false);
                                                              },
                                                              child:
                                                                  new Container(
                                                                padding:
                                                                    new EdgeInsets
                                                                            .all(
                                                                        3.0),
                                                                decoration:
                                                                    new BoxDecoration(
                                                                  shape: BoxShape
                                                                      .circle,
                                                                  gradient: new LinearGradient(
                                                                      colors: [
                                                                        const Color(
                                                                            0xF29E30BF),
                                                                        const Color(
                                                                            0xF2262BA0)
                                                                      ],
                                                                      begin: const FractionalOffset(
                                                                          0.0, 0.0),
                                                                      end: const FractionalOffset(
                                                                          1.0,
                                                                          0.0),
                                                                      stops: [
                                                                        0.0,
                                                                        1.0
                                                                      ],
                                                                      tileMode:
                                                                          TileMode
                                                                              .clamp),
                                                                ),
                                                                child: new Icon(
                                                                  Icons.remove,
                                                                  color: Colors
                                                                      .white,
                                                                  size: 20.0,
                                                                ),
                                                              ),
                                                            ),
                                                            new Container(
                                                              padding:
                                                                  new EdgeInsets
                                                                      .only(
                                                                left: 12.0,
                                                                right: 12.0,
                                                                top: 5.0,
                                                                bottom: 5.0,
                                                              ),
                                                              decoration:
                                                                  new BoxDecoration(
                                                                      border:
                                                                          new Border
                                                                              .all(
                                                                color: const Color(
                                                                    0xF29E30BF),
                                                              )),
                                                              child: new Text(
                                                                list[index]
                                                                    .quantityInCart
                                                                    .toString(),
                                                                style:
                                                                    new TextStyle(
                                                                  fontSize:
                                                                      14.0,
                                                                ),
                                                              ),
                                                            ),
                                                            new GestureDetector(
                                                              onTap: () {
                                                                updateCartItem(
                                                                    list[index],
                                                                    index,
                                                                    true);
                                                              },
                                                              child:
                                                                  new Container(
                                                                padding:
                                                                    new EdgeInsets
                                                                            .all(
                                                                        3.0),
                                                                decoration:
                                                                    new BoxDecoration(
                                                                  shape: BoxShape
                                                                      .circle,
                                                                  gradient: new LinearGradient(
                                                                      colors: [
                                                                        const Color(
                                                                            0xF29E30BF),
                                                                        const Color(
                                                                            0xF2262BA0)
                                                                      ],
                                                                      begin: const FractionalOffset(
                                                                          0.0, 0.0),
                                                                      end: const FractionalOffset(
                                                                          1.0,
                                                                          0.0),
                                                                      stops: [
                                                                        0.0,
                                                                        1.0
                                                                      ],
                                                                      tileMode:
                                                                          TileMode
                                                                              .clamp),
                                                                ),
                                                                child: new Icon(
                                                                  Icons.add,
                                                                  color: Colors
                                                                      .white,
                                                                  size: 20.0,
                                                                ),
                                                              ),
                                                            ),
                                                          ],
                                                        ),
                                                      ),
                                              ),
                                            ],
                                          ),
                                        ),
                                      )
                                    : new Center(
                                        child: new CircularProgressIndicator(
                                          valueColor:
                                              new AlwaysStoppedAnimation(
                                                  const Color(0xF29E30BF)),
                                        ),
                                      );
                              },
                              childCount: list.length,
                            ),
                          ),
                        )
                      : isEmptyCheck == false
                          ? new SliverFillRemaining(
                              child: new Center(
                                child: new CircularProgressIndicator(
                                  valueColor: new AlwaysStoppedAnimation(
                                      const Color(0xF29E30BF)),
                                ),
                              ),
                            )
                          : SliverFillRemaining(
                              child: new Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  new Image.asset(
                                    "assets/empty.png",
                                    height: 100.0,
                                    width: 100.0,
                                  ),
                                  new Text(
                                    "No item",
                                    style: new TextStyle(
                                      fontWeight: FontWeight.bold,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
