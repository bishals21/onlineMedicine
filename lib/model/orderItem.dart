class OrderItem {
  OrderItem(this.id, this.trackingNumber, this.adderss, this.status,
      this.prescription, this.createdOn, this.updatedOn, this.itemList);
  int id;
  String trackingNumber;
  Address adderss;
  String status;
  List<Prescription> prescription;
  String createdOn;
  String updatedOn;
  List<Item> itemList;
}

class Address {
  Address(this.tole, this.city, this.address, this.latitude, this.longitude);
  String tole;
  String city;
  String address;
  String latitude;
  String longitude;
}

class Item {
  Item(this.id, this.name, this.rate, this.appliedRate, this.quantity,
      this.total);
  int id;
  String name;
  String rate;
  String appliedRate;
  int quantity;
  String total;
}

class Prescription {
  Prescription(this.id, this.file);
  int id;
  String file;
}
