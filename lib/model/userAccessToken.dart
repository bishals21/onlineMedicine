class UserAccessToken {
  String accessToken, name, phone, email, address, dob;

  UserAccessToken(
    this.accessToken,
    this.name,
    this.phone,
    this.email,
    this.address,
    this.dob,
  );

  Map<String, dynamic> toJson() {
    var map = new Map<String, dynamic>();
    map["accessToken"] = accessToken;
    map["name"] = name;
    map["phone"] = phone;
    map["email"] = email;
    map["address"] = address;
    map["dob"] = dob;
    return map;
  }

  UserAccessToken.fromJson(Map<String, dynamic> map)
      : this(
          map["accessToken"],
          map["name"],
          map["phone"],
          map["email"],
          map["address"],
          map["dob"],
        );
}
