class OnlineMedicineCategory {
  OnlineMedicineCategory(
      this.id, this.name, this.slug, this.image, this.description);
  int id;
  String name;
  String slug;
  String image;
  String description;
}
