class OnlineMedicineItem {
  OnlineMedicineItem(
    this.id,
    this.name,
    this.image,
    this.description,
    this.company,
    this.companyId,
    this.category,
    this.categoryId,
    this.compositions,
    this.rate,
    this.offer,
    this.applicableRate,
    this.rateDescription,
    this.isPrescriptionRequired,
    this.quantityInCart,
  );

  updateAddToCart(int quantity) {
    this.quantityInCart = quantity;
  }

  int id;
  String name;
  String image;
  String description;
  String company;
  int companyId;
  String category;
  String categoryId;
  String compositions;
  String rate;
  String offer;
  String applicableRate;
  String rateDescription;
  bool isPrescriptionRequired;
  int quantityInCart;

  Map<String, dynamic> toJson() {
    var map = new Map<String, dynamic>();
    map["id"] = id;
    map["name"] = name;
    map["image"] = image;
    map["description"] = description;
    map["company"] = company;
    map["companyId"] = companyId;
    map["category"] = category;
    map["categoryId"] = categoryId;
    map["compositions"] = compositions;
    map["rate"] = rate;
    map["offer"] = offer;
    map["applicableRate"] = applicableRate;
    map["rateDescription"] = rateDescription;
    map["isPrescriptionRequired"] = isPrescriptionRequired;
    map["quantityInCart"] = quantityInCart;
    return map;
  }

  OnlineMedicineItem.fromJson(Map<String, dynamic> map)
      : this(
          map["id"],
          map["name"],
          map["image"],
          map["description"],
          map["company"],
          map["companyId"],
          map["category"],
          map["categoryId"],
          map["compositions"],
          map["rate"],
          map["offer"],
          map["applicableRate"],
          map["rateDescription"],
          map["isPrescriptionRequired"],
          map["quantityInCart"],
        );
}
