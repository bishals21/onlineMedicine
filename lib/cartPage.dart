import 'package:flutter/material.dart';
import 'package:online_medicine/appText.dart';
import 'package:online_medicine/appbar/gradientAppBarWithBack.dart';
import 'package:online_medicine/checkoutPage.dart';
import 'package:online_medicine/loginPage.dart';
import 'package:online_medicine/model/onlineMedicineItem.dart';
import 'package:online_medicine/model/userAccessToken.dart';
import 'package:online_medicine/stateContainer/stateContainer.dart';

class CartPage extends StatefulWidget {
  @override
  _CartPageState createState() => _CartPageState();
}

class _CartPageState extends State<CartPage> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    var container = StateContainer.of(context);
    List<OnlineMedicineItem> cartList = container.cartItemList;
    List<OnlineMedicineItem> list = container.medicineList;
    List<OnlineMedicineItem> specialOfferList = container.specialOfferList;
    UserAccessToken userAccessToken = container.userAccessToken;

    double totalPrice = 0.0;
    cartList.forEach((item) {
      totalPrice = totalPrice +
          (double.parse(item.applicableRate) * item.quantityInCart);
    });

    updateCartItem(OnlineMedicineItem item, int index, bool method) {
      OnlineMedicineItem updateItem = item;
      int quantity = updateItem.quantityInCart;
      if (method == true) {
        updateItem.quantityInCart = quantity + 1;
        cartList.removeAt(index);
        cartList.insert(index, updateItem);
        for (int j = 0; j < list.length; j++) {
          if (updateItem.id == list[j].id) {
            list.removeAt(j);
            list.insert(j, updateItem);
          }
        }
        for (int s = 0; s < specialOfferList.length; s++) {
          if (updateItem.id == specialOfferList[s].id) {
            specialOfferList.removeAt(s);
            specialOfferList.insert(s, updateItem);
          }
        }
        container.updateSpecialOfferList(specialOfferList, false);
        container.updateMedicineList(list, false);
        container.updateCartList(cartList);
      } else {
        if (quantity == 1) {
          updateItem.quantityInCart = 0;
          for (int j = 0; j < list.length; j++) {
            if (updateItem.id == list[j].id) {
              list.removeAt(j);
              list.insert(j, updateItem);
            }
          }
          for (int s = 0; s < specialOfferList.length; s++) {
            if (updateItem.id == specialOfferList[s].id) {
              specialOfferList.removeAt(s);
              specialOfferList.insert(s, updateItem);
            }
          }
          cartList.removeWhere((items) => items.id == updateItem.id);
          container.updateSpecialOfferList(specialOfferList, false);
          container.updateMedicineList(list, false);
          container.updateCartList(cartList);
        } else {
          updateItem.quantityInCart = quantity - 1;
          for (int j = 0; j < list.length; j++) {
            if (updateItem.id == list[j].id) {
              list.removeAt(j);
              list.insert(j, updateItem);
            }
          }
          for (int s = 0; s < specialOfferList.length; s++) {
            if (updateItem.id == specialOfferList[s].id) {
              specialOfferList.removeAt(s);
              specialOfferList.insert(s, updateItem);
            }
          }

          cartList.removeAt(index);
          cartList.insert(index, updateItem);
          container.updateSpecialOfferList(specialOfferList, false);
          container.updateMedicineList(list, false);
          container.updateCartList(cartList);
        }
      }
    }

    return new Scaffold(
      key: _scaffoldKey,
      body: new Column(
        children: <Widget>[
          new GradientAppBarWithBack("My Cart"),
          new Expanded(
            child: new CustomScrollView(
              slivers: <Widget>[
                new SliverPadding(
                  padding: new EdgeInsets.all(8.0),
                  sliver: new SliverFixedExtentList(
                    itemExtent: 145.0,
                    delegate: new SliverChildBuilderDelegate(
                      (context, index) {
                        return new Container(
                          decoration: new BoxDecoration(
                              border: new Border(
                                  bottom: new BorderSide(
                            width: 0.5,
                          ))),
                          padding: new EdgeInsets.all(8.0),
                          child: new Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              new Row(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  new Container(
                                    height: 90.0,
                                    width: 90.0,
                                    decoration: new BoxDecoration(
                                      border: new Border.all(
                                        color: Colors.grey,
                                        width: 0.7,
                                      ),
                                      borderRadius: BorderRadius.all(
                                          new Radius.circular(8.0)),
                                    ),
                                    child: new Center(
                                      child: cartList[index].image != null
                                          ? new Image.network(
                                              AppText.prefixImage +
                                                  cartList[index].image,
                                              height: 65.0,
                                              width: 65.0,
                                            )
                                          : new Image.asset(
                                              "assets/medication.png",
                                              height: 65.0,
                                              width: 65.0,
                                            ),
                                    ),
                                  ),
                                  new Flexible(
                                    //padding: const EdgeInsets.all(8.0),
                                    child: Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: new Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: <Widget>[
                                          new Text(
                                            cartList[index].name,
                                            maxLines: 1,
                                            overflow: TextOverflow.ellipsis,
                                            style: new TextStyle(
                                              fontSize: 16.0,
                                              fontWeight: FontWeight.bold,
                                            ),
                                          ),
                                          new Padding(
                                            padding:
                                                new EdgeInsets.only(top: 5.0),
                                          ),
                                          new Text(
                                            cartList[index].company,
                                            maxLines: 1,
                                            overflow: TextOverflow.ellipsis,
                                            style: new TextStyle(
                                              fontSize: 15.0,
                                            ),
                                          ),
                                          new Padding(
                                            padding:
                                                new EdgeInsets.only(top: 5.0),
                                          ),
                                          new Row(
                                            children: <Widget>[
                                              new Text(
                                                "Rs. " + cartList[index].rate,
                                                style: new TextStyle(
                                                  fontWeight: FontWeight.bold,
                                                  color: Colors.red,
                                                  decoration: TextDecoration
                                                      .lineThrough,
                                                ),
                                              ),
                                              new Padding(
                                                padding: new EdgeInsets.only(
                                                    left: 16.0),
                                              ),
                                              new Text(
                                                "Rs. " +
                                                    cartList[index]
                                                        .applicableRate,
                                                style: new TextStyle(
                                                  fontWeight: FontWeight.bold,
                                                ),
                                              ),
                                            ],
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                              new Align(
                                  alignment: Alignment.bottomRight,
                                  child: new Container(
                                    width: 110.0,
                                    child: new Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: <Widget>[
                                        new GestureDetector(
                                          onTap: () {
                                            updateCartItem(
                                                cartList[index], index, false);
                                          },
                                          child: new Container(
                                            padding: new EdgeInsets.all(3.0),
                                            decoration: new BoxDecoration(
                                              shape: BoxShape.circle,
                                              gradient: new LinearGradient(
                                                  colors: [
                                                    const Color(0xF29E30BF),
                                                    const Color(0xF2262BA0)
                                                  ],
                                                  begin: const FractionalOffset(
                                                      0.0, 0.0),
                                                  end: const FractionalOffset(
                                                      1.0, 0.0),
                                                  stops: [0.0, 1.0],
                                                  tileMode: TileMode.clamp),
                                            ),
                                            child: new Icon(
                                              Icons.remove,
                                              color: Colors.white,
                                              size: 20.0,
                                            ),
                                          ),
                                        ),
                                        new Container(
                                          padding: new EdgeInsets.only(
                                            left: 12.0,
                                            right: 12.0,
                                            top: 5.0,
                                            bottom: 5.0,
                                          ),
                                          decoration: new BoxDecoration(
                                              border: new Border.all(
                                            color: const Color(0xF29E30BF),
                                          )),
                                          child: new Text(
                                            cartList[index]
                                                .quantityInCart
                                                .toString(),
                                            style: new TextStyle(
                                              fontSize: 14.0,
                                            ),
                                          ),
                                        ),
                                        new GestureDetector(
                                          onTap: () {
                                            updateCartItem(
                                                cartList[index], index, true);
                                          },
                                          child: new Container(
                                            padding: new EdgeInsets.all(3.0),
                                            decoration: new BoxDecoration(
                                              shape: BoxShape.circle,
                                              gradient: new LinearGradient(
                                                  colors: [
                                                    const Color(0xF29E30BF),
                                                    const Color(0xF2262BA0)
                                                  ],
                                                  begin: const FractionalOffset(
                                                      0.0, 0.0),
                                                  end: const FractionalOffset(
                                                      1.0, 0.0),
                                                  stops: [0.0, 1.0],
                                                  tileMode: TileMode.clamp),
                                            ),
                                            child: new Icon(
                                              Icons.add,
                                              color: Colors.white,
                                              size: 20.0,
                                            ),
                                          ),
                                        ),
                                      ],
                                    ),
                                  )),
                            ],
                          ),
                        );
                      },
                      childCount: cartList.length,
                    ),
                  ),
                ),
              ],
            ),
          ),
          new Container(
            height: 60.0,
            padding: new EdgeInsets.only(left: 16.0, right: 16.0),
            decoration: new BoxDecoration(
              gradient: new LinearGradient(
                  colors: [const Color(0xF29E30BF), const Color(0xF2262BA0)],
                  begin: const FractionalOffset(0.0, 0.0),
                  end: const FractionalOffset(1.0, 0.0),
                  stops: [0.0, 1.0],
                  tileMode: TileMode.clamp),
            ),
            child: cartList.isNotEmpty
                ? new Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      new Text(
                        "Total \n Rs. " + totalPrice.toStringAsFixed(2),
                        textAlign: TextAlign.center,
                        style: new TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                      new GestureDetector(
                        onTap: () {
                          Navigator.push(
                              context,
                              new MaterialPageRoute(
                                  builder: (context) => userAccessToken == null
                                      ? new LoginPage()
                                      : new CheckoutPage()));
                        },
                        child: new Chip(
                          labelStyle: new TextStyle(
                            fontWeight: FontWeight.bold,
                            color: Colors.black,
                          ),
                          backgroundColor: Colors.white,
                          label: new Text("CHECKOUT"),
                        ),
                      ),
                    ],
                  )
                : new Center(
                    child: new Text(
                      "Cart is empty",
                      style: new TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.w600,
                      ),
                    ),
                  ),
          ),
        ],
      ),
    );
  }
}
