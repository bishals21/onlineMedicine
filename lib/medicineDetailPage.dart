import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:online_medicine/appText.dart';
import 'package:online_medicine/appbar/gradientWithCart.dart';
import 'package:online_medicine/medicineDetailEndPage.dart';
import 'package:online_medicine/model/onlineMedicineItem.dart';
import 'package:online_medicine/stateContainer/stateContainer.dart';
import 'package:http/http.dart' as http;
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';

class MedicineDetailRoute extends CupertinoPageRoute {
  final OnlineMedicineItem onlineMedicineItem;
  MedicineDetailRoute(this.onlineMedicineItem)
      : super(
            builder: (BuildContext context) => new MedicineDetailPage(
                  onlineMedicineItem: onlineMedicineItem,
                ));

  @override
  Widget buildPage(BuildContext context, Animation<double> animation,
      Animation<double> secondaryAnimation) {
    return new FadeTransition(
      opacity: animation,
      child: new MedicineDetailPage(
        onlineMedicineItem: onlineMedicineItem,
      ),
    );
  }
}

class MedicineDetailPage extends StatefulWidget {
  final OnlineMedicineItem onlineMedicineItem;

  MedicineDetailPage({Key key, @required this.onlineMedicineItem})
      : super(key: key);
  @override
  _MedicineDetailPageState createState() => _MedicineDetailPageState();
}

class _MedicineDetailPageState extends State<MedicineDetailPage> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  OnlineMedicineItem onlineMedicineItem;

  getSubstitutes(int id) async {
    final container = StateContainer.of(context);
    List<OnlineMedicineItem> listOfSubstitutes = new List();
    container.updateSubstituteList([], true);
    bool isConnected = await checkConnection();
    if (isConnected) {
      http.get(
          AppText.baseURl +
              "medicine/substitute/" +
              id.toString() +
              "?_format=json",
          headers: {
            "api-key": AppText.apiKey,
          }).then((response) {
        if (response.statusCode == 200) {
          var responseBody = json.decode(response.body);
          var medicines = responseBody["data"]["medicines"];
          for (var item in medicines) {
            listOfSubstitutes.add(new OnlineMedicineItem(
              item["id"],
              item["name"],
              item["image"],
              item["description"],
              item["company"],
              item["companyId"],
              item["category"],
              item["categoryId"].toString(),
              item["composition"],
              item["rate"].toString(),
              item["offer"].toString(),
              item["applicableRate"].toString(),
              item["rateDescription"],
              item["isPrescriptionRequired"],
              0,
            ));
          }

          for (int i = 0; i < container.cartItemList.length; i++) {
            for (int j = 0; j < listOfSubstitutes.length; j++) {
              if (container.cartItemList[i].id == listOfSubstitutes[j].id) {
                OnlineMedicineItem item = listOfSubstitutes[j];
                item.updateAddToCart(container.cartItemList[i].quantityInCart);
                listOfSubstitutes.removeAt(j);
                listOfSubstitutes.insert(j, item);
              }
            }
          }

          container.updateSubstituteList(listOfSubstitutes, true);
        }
      });
    } else {
      _showSnackBar("No Internet Connection");
    }
  }

  Future<bool> checkConnection() async {
    bool connected = false;
    try {
      final result = await InternetAddress.lookup('google.com');
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        print('connected');
        connected = true;
      }
    } on SocketException catch (_) {
      print('not connected');
      connected = false;
    }

    return connected;
  }

  void _showSnackBar(String text) {
    _scaffoldKey.currentState.showSnackBar(new SnackBar(
      content: new Text(text),
    ));
  }

  @override
  void initState() {
    onlineMedicineItem = widget.onlineMedicineItem;
    new Future.delayed(Duration.zero, () {
      getSubstitutes(onlineMedicineItem.id);
    });

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final container = StateContainer.of(context);
    List<OnlineMedicineItem> cartList = container.cartItemList;
    List<OnlineMedicineItem> medicineList = container.medicineList;
    List<OnlineMedicineItem> substituteList = container.substituteList;
    List<String> compositionList = new List();

    addItemToCart() {
      onlineMedicineItem.quantityInCart = 1;
      for (int j = 0; j < medicineList.length; j++) {
        if (onlineMedicineItem.id == medicineList[j].id) {
          medicineList.removeAt(j);
          medicineList.insert(j, onlineMedicineItem);
        }
      }
      container.updateMedicineList(medicineList, false);
      cartList.add(onlineMedicineItem);
      container.updateCartList(cartList);
    }

    addSubsToCart(OnlineMedicineItem item, int index) {
      OnlineMedicineItem addedItem = item;
      addedItem.quantityInCart = 1;
      substituteList.removeAt(index);
      substituteList.insert(index, addedItem);
      for (int j = 0; j < medicineList.length; j++) {
        if (addedItem.id == medicineList[j].id) {
          medicineList.removeAt(j);
          medicineList.insert(j, addedItem);
        }
      }
      container.updateMedicineList(medicineList, false);
      cartList.add(item);
      container.updateCartList(cartList);
    }

    updateSubsCartItem(OnlineMedicineItem item, int index, bool method) {
      OnlineMedicineItem updateItem = item;
      int quantity = updateItem.quantityInCart;
      if (method == true) {
        updateItem.quantityInCart = quantity + 1;
        substituteList.removeAt(index);
        substituteList.insert(index, updateItem);
        container.updateSubstituteList(substituteList, false);
        for (int j = 0; j < medicineList.length; j++) {
          if (updateItem.id == medicineList[j].id) {
            medicineList.removeAt(j);
            medicineList.insert(j, updateItem);
          }
        }
        container.updateMedicineList(medicineList, false);
        cartList.forEach((item) {
          if (item.id == updateItem.id) {
            cartList.remove(item);
            cartList.add(updateItem);
          }
        });
        container.updateCartList(cartList);
      } else {
        if (quantity == 1) {
          updateItem.quantityInCart = 0;
          substituteList.removeAt(index);
          substituteList.insert(index, updateItem);
          container.updateSubstituteList(substituteList, false);
          for (int j = 0; j < medicineList.length; j++) {
            if (updateItem.id == medicineList[j].id) {
              medicineList.removeAt(j);
              medicineList.insert(j, updateItem);
            }
          }
          container.updateMedicineList(medicineList, false);
          cartList.removeWhere((item) => item.id == updateItem.id);
          container.updateCartList(cartList);
        } else {
          updateItem.quantityInCart = quantity - 1;
          substituteList.removeAt(index);
          substituteList.insert(index, updateItem);
          container.updateSubstituteList(substituteList, false);
          for (int j = 0; j < medicineList.length; j++) {
            if (updateItem.id == medicineList[j].id) {
              medicineList.removeAt(j);
              medicineList.insert(j, updateItem);
            }
          }
          container.updateMedicineList(medicineList, false);
          cartList.forEach((item) {
            if (item.id == updateItem.id) {
              cartList.remove(item);
              cartList.add(updateItem);
            }
          });
          container.updateCartList(cartList);
        }
      }
    }

    updateItemInCart(bool method) {
      int quantity = onlineMedicineItem.quantityInCart;
      if (method == true) {
        onlineMedicineItem.quantityInCart = quantity + 1;
        for (int j = 0; j < medicineList.length; j++) {
          if (onlineMedicineItem.id == medicineList[j].id) {
            medicineList.removeAt(j);
            medicineList.insert(j, onlineMedicineItem);
          }
        }
        container.updateMedicineList(medicineList, false);
        cartList.forEach((item) {
          if (item.id == onlineMedicineItem.id) {
            cartList.remove(item);
            cartList.add(onlineMedicineItem);
          }
        });
        container.updateCartList(cartList);
      } else {
        if (quantity == 1) {
          onlineMedicineItem.quantityInCart = 0;
          for (int j = 0; j < medicineList.length; j++) {
            if (onlineMedicineItem.id == medicineList[j].id) {
              medicineList.removeAt(j);
              medicineList.insert(j, onlineMedicineItem);
            }
          }
          container.updateMedicineList(medicineList, false);
          cartList.removeWhere((item) => item.id == onlineMedicineItem.id);
          container.updateCartList(cartList);
        } else {
          onlineMedicineItem.quantityInCart = quantity - 1;
          for (int j = 0; j < medicineList.length; j++) {
            if (onlineMedicineItem.id == medicineList[j].id) {
              medicineList.removeAt(j);
              medicineList.insert(j, onlineMedicineItem);
            }
          }
          container.updateMedicineList(medicineList, false);
          cartList.forEach((item) {
            if (item.id == onlineMedicineItem.id) {
              cartList.remove(item);
              cartList.add(onlineMedicineItem);
            }
          });
          container.updateCartList(cartList);
        }
      }
    }

    if (onlineMedicineItem.compositions != null) {
      compositionList = onlineMedicineItem.compositions.split(",");
    }

    return new Scaffold(
      body: new Column(
        children: <Widget>[
          new GradientAppBarWithCart(onlineMedicineItem.name, cartList, false),
          new Expanded(
            child: new ListView(
              padding: new EdgeInsets.all(8.0),
              children: <Widget>[
                new Container(
                  padding: new EdgeInsets.all(8.0),
                  decoration: new BoxDecoration(
                    border: new Border.all(color: Colors.grey, width: 0.7),
                  ),
                  child: new Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      new Container(
                        height: 200.0,
                        child: new Center(
                          child: onlineMedicineItem.image != null
                              ? new Hero(
                                  tag: "image" +
                                      onlineMedicineItem.id.toString(),
                                  child: new Image.network(
                                    AppText.prefixImage +
                                        onlineMedicineItem.image,
                                    height: 150.0,
                                    width: 150.0,
                                  ),
                                )
                              : new Hero(
                                  tag: "image" +
                                      onlineMedicineItem.id.toString(),
                                  child: new Image.asset(
                                    "assets/medication.png",
                                    height: 150.0,
                                    width: 150.0,
                                  ),
                                ),
                        ),
                      ),
                      new Divider(
                        color: Colors.grey,
                        height: 20.0,
                      ),
                      new Hero(
                        tag: "name" + onlineMedicineItem.id.toString(),
                        child: new Material(
                          color: Colors.transparent,
                          child: new Text(
                            onlineMedicineItem.name,
                            style: new TextStyle(
                              fontSize: 16.0,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                      ),
                      new Padding(
                        padding: new EdgeInsets.only(top: 8.0),
                      ),
                      new Hero(
                        tag: "company" + onlineMedicineItem.id.toString(),
                        child: new Material(
                          color: Colors.transparent,
                          child: new Text(
                            onlineMedicineItem.company,
                            style: new TextStyle(),
                          ),
                        ),
                      ),
                      new Padding(
                        padding: new EdgeInsets.only(top: 8.0),
                      ),
                      new Text(
                        "Compositions: ",
                        style: new TextStyle(
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      new Padding(
                        padding: new EdgeInsets.only(top: 8.0),
                      ),
                      new Column(
                        children: compositionList.map((item) {
                          return new Container(
                            padding: EdgeInsets.all(5.0),
                            child: Row(
                              children: <Widget>[
                                new Container(
                                  height: 8.0,
                                  width: 8.0,
                                  margin:
                                      new EdgeInsets.symmetric(horizontal: 8.0),
                                  decoration: new BoxDecoration(
                                    color: Colors.black54,
                                    shape: BoxShape.circle,
                                  ),
                                ),
                                new Text(item),
                              ],
                            ),
                          );
                        }).toList(),
                      ),
                      new Padding(
                        padding: new EdgeInsets.only(top: 16.0),
                      ),
                      new Row(
                        children: <Widget>[
                          new Hero(
                            tag: "rate" + "${onlineMedicineItem.id}",
                            child: new Material(
                              color: Colors.transparent,
                              child: new Text(
                                "Rs. " + onlineMedicineItem.rate,
                                style: new TextStyle(
                                  fontWeight: FontWeight.bold,
                                  color: Colors.red,
                                  decoration: TextDecoration.lineThrough,
                                ),
                              ),
                            ),
                          ),
                          new Padding(
                            padding: new EdgeInsets.only(left: 16.0),
                          ),
                          new Hero(
                            tag: "applicableRate" + "${onlineMedicineItem.id}",
                            child: new Material(
                              color: Colors.transparent,
                              child: new Text(
                                "Rs. " + onlineMedicineItem.applicableRate,
                                style: new TextStyle(
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                      new Align(
                          alignment: Alignment.centerRight,
                          child: onlineMedicineItem.quantityInCart == 0
                              ? new InkWell(
                                  splashColor: Colors.black,
                                  onTap: () {
                                    addItemToCart();
                                  },
                                  child: new Container(
                                      height: 35.0,
                                      width: 110.0,
                                      decoration: new BoxDecoration(
                                        gradient: new LinearGradient(
                                            colors: [
                                              const Color(0xF29E30BF),
                                              const Color(0xF2262BA0)
                                            ],
                                            begin: const FractionalOffset(
                                                0.0, 0.0),
                                            end: const FractionalOffset(
                                                1.0, 0.0),
                                            stops: [0.0, 1.0],
                                            tileMode: TileMode.clamp),
                                      ),
                                      child: new Center(
                                        child: new Text(
                                          "ADD TO CART",
                                          style: new TextStyle(
                                            color: Colors.white,
                                            fontWeight: FontWeight.w600,
                                          ),
                                        ),
                                      )))
                              : new Container(
                                  width: 110.0,
                                  child: new Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: <Widget>[
                                      new GestureDetector(
                                        onTap: () {
                                          updateItemInCart(false);
                                        },
                                        child: new Container(
                                          padding: new EdgeInsets.all(3.0),
                                          decoration: new BoxDecoration(
                                            shape: BoxShape.circle,
                                            gradient: new LinearGradient(
                                                colors: [
                                                  const Color(0xF29E30BF),
                                                  const Color(0xF2262BA0)
                                                ],
                                                begin: const FractionalOffset(
                                                    0.0, 0.0),
                                                end: const FractionalOffset(
                                                    1.0, 0.0),
                                                stops: [0.0, 1.0],
                                                tileMode: TileMode.clamp),
                                          ),
                                          child: new Icon(
                                            Icons.remove,
                                            color: Colors.white,
                                            size: 20.0,
                                          ),
                                        ),
                                      ),
                                      new Container(
                                        padding: new EdgeInsets.only(
                                          left: 12.0,
                                          right: 12.0,
                                          top: 5.0,
                                          bottom: 5.0,
                                        ),
                                        decoration: new BoxDecoration(
                                            border: new Border.all(
                                          color: const Color(0xF29E30BF),
                                        )),
                                        child: new Text(
                                          onlineMedicineItem.quantityInCart
                                              .toString(),
                                          style: new TextStyle(
                                            fontSize: 14.0,
                                          ),
                                        ),
                                      ),
                                      new GestureDetector(
                                        onTap: () {
                                          updateItemInCart(true);
                                        },
                                        child: new Container(
                                          padding: new EdgeInsets.all(3.0),
                                          decoration: new BoxDecoration(
                                            shape: BoxShape.circle,
                                            gradient: new LinearGradient(
                                                colors: [
                                                  const Color(0xF29E30BF),
                                                  const Color(0xF2262BA0)
                                                ],
                                                begin: const FractionalOffset(
                                                    0.0, 0.0),
                                                end: const FractionalOffset(
                                                    1.0, 0.0),
                                                stops: [0.0, 1.0],
                                                tileMode: TileMode.clamp),
                                          ),
                                          child: new Icon(
                                            Icons.add,
                                            color: Colors.white,
                                            size: 20.0,
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                )),
                      onlineMedicineItem.description != null
                          ? new Divider(
                              color: Colors.grey,
                              height: 20.0,
                            )
                          : new Container(),
                      new Padding(
                        padding: new EdgeInsets.only(top: 8.0),
                      ),
                      onlineMedicineItem.description != null
                          ? new Text(
                              "Product Detail",
                              style: new TextStyle(
                                fontSize: 16.0,
                                fontWeight: FontWeight.bold,
                              ),
                            )
                          : new Container(),
                      new Padding(
                        padding: new EdgeInsets.only(top: 8.0),
                      ),
                      onlineMedicineItem.description != null
                          ? new Align(
                              alignment: Alignment.centerRight,
                              child: new InkWell(
                                splashColor: Colors.black,
                                onTap: () {
                                  print(onlineMedicineItem.description);
                                  Navigator.of(context).push(
                                      new MaterialPageRoute(
                                          builder: (context) =>
                                              new WebviewScaffold(
                                                url: new Uri.dataFromString(
                                                        onlineMedicineItem
                                                            .description,
                                                        mimeType: 'text/html')
                                                    .toString(),
                                                appBar: new AppBar(
                                                  backgroundColor:
                                                      Color(0xFF9E30BF),
                                                  title: new Text(
                                                    "Product Detail",
                                                  ),
                                                ),
                                              ),
                                          fullscreenDialog: true));
                                },
                                child: new Container(
                                  height: 35.0,
                                  width: 110.0,
                                  decoration: new BoxDecoration(
                                    gradient: new LinearGradient(
                                        colors: [
                                          const Color(0xF29E30BF),
                                          const Color(0xF2262BA0)
                                        ],
                                        begin: const FractionalOffset(0.0, 0.0),
                                        end: const FractionalOffset(1.0, 0.0),
                                        stops: [0.0, 1.0],
                                        tileMode: TileMode.clamp),
                                  ),
                                  child: new Center(
                                    child: new Text(
                                      "View Detail",
                                      style: new TextStyle(
                                        color: Colors.white,
                                        fontWeight: FontWeight.w600,
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            )
                          : new Container(),
                    ],
                  ),
                ),
                new Padding(
                  padding: new EdgeInsets.only(top: 16.0),
                ),
                substituteList.isNotEmpty
                    ? new Container(
                        width: 100.0,
                        padding: new EdgeInsets.all(8.0),
                        decoration: new BoxDecoration(
                            border: new Border.all(
                          width: 1.5,
                          color: const Color(0xF2262BA0),
                        )),
                        child: new Text(
                          "SUBSTITUTES",
                          textAlign: TextAlign.center,
                          style: new TextStyle(
                            fontSize: 18.0,
                            color: const Color(0xF2262BA0),
                            //fontWeight: FontWeight.bold,
                          ),
                        ),
                      )
                    : new Container(),
                new ListView.builder(
                  padding: new EdgeInsets.all(8.0),
                  itemCount: substituteList.length,
                  shrinkWrap: true,
                  physics: new ClampingScrollPhysics(),
                  itemBuilder: (context, index) {
                    return new GestureDetector(
                      onTap: () {
                        //Navigator.pop(context);
                        Navigator.push(
                          context,
                          new MaterialPageRoute(
                            builder: (context) => new MedicineDetailEndPage(
                                  onlineMedicineItem: substituteList[index],
                                ),
                            fullscreenDialog: true,
                          ),
                        );
                      },
                      child: new Container(
                        decoration: new BoxDecoration(
                            border: new Border(
                                bottom: new BorderSide(
                          width: 0.5,
                        ))),
                        padding: new EdgeInsets.all(8.0),
                        child: new Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            new Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                new Hero(
                                  tag: substituteList[index].id,
                                  child: new Container(
                                    height: 90.0,
                                    width: 90.0,
                                    decoration: new BoxDecoration(
                                      border: new Border.all(
                                        color: Colors.grey,
                                        width: 0.7,
                                      ),
                                      borderRadius: BorderRadius.all(
                                          new Radius.circular(8.0)),
                                    ),
                                    child: new Center(
                                      child: substituteList[index].image != null
                                          ? new Image.network(
                                              AppText.prefixImage +
                                                  substituteList[index].image,
                                              height: 65.0,
                                              width: 65.0,
                                            )
                                          : new Image.asset(
                                              "assets/medication.png",
                                              height: 65.0,
                                              width: 65.0,
                                            ),
                                    ),
                                  ),
                                ),
                                new Flexible(
                                  //padding: const EdgeInsets.all(8.0),
                                  child: Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: new Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: <Widget>[
                                        new Text(
                                          substituteList[index].name,
                                          maxLines: 1,
                                          overflow: TextOverflow.ellipsis,
                                          style: new TextStyle(
                                            fontSize: 16.0,
                                            fontWeight: FontWeight.bold,
                                          ),
                                        ),
                                        new Padding(
                                          padding:
                                              new EdgeInsets.only(top: 5.0),
                                        ),
                                        new Text(
                                          substituteList[index].company,
                                          maxLines: 1,
                                          overflow: TextOverflow.ellipsis,
                                          style: new TextStyle(
                                            fontSize: 15.0,
                                          ),
                                        ),
                                        new Padding(
                                          padding:
                                              new EdgeInsets.only(top: 5.0),
                                        ),
                                        onlineMedicineItem.rate ==
                                                onlineMedicineItem
                                                    .applicableRate
                                            ? new Text(
                                                "Rs. " +
                                                    onlineMedicineItem
                                                        .applicableRate,
                                                style: new TextStyle(
                                                  fontWeight: FontWeight.bold,
                                                ),
                                              )
                                            : new Row(
                                                children: <Widget>[
                                                  new Text(
                                                    "Rs. " +
                                                        onlineMedicineItem.rate,
                                                    style: new TextStyle(
                                                      fontWeight:
                                                          FontWeight.bold,
                                                      color: Colors.red,
                                                      decoration: TextDecoration
                                                          .lineThrough,
                                                    ),
                                                  ),
                                                  new Padding(
                                                    padding:
                                                        new EdgeInsets.only(
                                                            left: 16.0),
                                                  ),
                                                  new Text(
                                                    "Rs. " +
                                                        onlineMedicineItem
                                                            .applicableRate,
                                                    style: new TextStyle(
                                                      fontWeight:
                                                          FontWeight.bold,
                                                    ),
                                                  ),
                                                ],
                                              ),
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),
                            new Align(
                                alignment: Alignment.bottomRight,
                                child: substituteList[index].quantityInCart == 0
                                    ? new InkWell(
                                        splashColor: Colors.black,
                                        onTap: () {
                                          addSubsToCart(
                                              substituteList[index], index);
                                        },
                                        child: new Container(
                                            height: 35.0,
                                            width: 110.0,
                                            decoration: new BoxDecoration(
                                              gradient: new LinearGradient(
                                                  colors: [
                                                    const Color(0xF29E30BF),
                                                    const Color(0xF2262BA0)
                                                  ],
                                                  begin: const FractionalOffset(
                                                      0.0, 0.0),
                                                  end: const FractionalOffset(
                                                      1.0, 0.0),
                                                  stops: [0.0, 1.0],
                                                  tileMode: TileMode.clamp),
                                            ),
                                            child: new Center(
                                              child: new Text(
                                                "ADD TO CART",
                                                style: new TextStyle(
                                                  color: Colors.white,
                                                  fontWeight: FontWeight.w600,
                                                ),
                                              ),
                                            )))
                                    : new Container(
                                        width: 110.0,
                                        child: new Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: <Widget>[
                                            new GestureDetector(
                                              onTap: () {
                                                updateSubsCartItem(
                                                    substituteList[index],
                                                    index,
                                                    false);
                                              },
                                              child: new Container(
                                                padding:
                                                    new EdgeInsets.all(3.0),
                                                decoration: new BoxDecoration(
                                                  shape: BoxShape.circle,
                                                  gradient: new LinearGradient(
                                                      colors: [
                                                        const Color(0xF29E30BF),
                                                        const Color(0xF2262BA0)
                                                      ],
                                                      begin:
                                                          const FractionalOffset(
                                                              0.0, 0.0),
                                                      end:
                                                          const FractionalOffset(
                                                              1.0, 0.0),
                                                      stops: [0.0, 1.0],
                                                      tileMode: TileMode.clamp),
                                                ),
                                                child: new Icon(
                                                  Icons.remove,
                                                  color: Colors.white,
                                                  size: 20.0,
                                                ),
                                              ),
                                            ),
                                            new Container(
                                              padding: new EdgeInsets.only(
                                                left: 12.0,
                                                right: 12.0,
                                                top: 5.0,
                                                bottom: 5.0,
                                              ),
                                              decoration: new BoxDecoration(
                                                  border: new Border.all(
                                                color: const Color(0xF29E30BF),
                                              )),
                                              child: new Text(
                                                substituteList[index]
                                                    .quantityInCart
                                                    .toString(),
                                                style: new TextStyle(
                                                  fontSize: 14.0,
                                                ),
                                              ),
                                            ),
                                            new GestureDetector(
                                              onTap: () {
                                                updateSubsCartItem(
                                                    substituteList[index],
                                                    index,
                                                    true);
                                              },
                                              child: new Container(
                                                padding:
                                                    new EdgeInsets.all(3.0),
                                                decoration: new BoxDecoration(
                                                  shape: BoxShape.circle,
                                                  gradient: new LinearGradient(
                                                      colors: [
                                                        const Color(0xF29E30BF),
                                                        const Color(0xF2262BA0)
                                                      ],
                                                      begin:
                                                          const FractionalOffset(
                                                              0.0, 0.0),
                                                      end:
                                                          const FractionalOffset(
                                                              1.0, 0.0),
                                                      stops: [0.0, 1.0],
                                                      tileMode: TileMode.clamp),
                                                ),
                                                child: new Icon(
                                                  Icons.add,
                                                  color: Colors.white,
                                                  size: 20.0,
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                                      )),
                          ],
                        ),
                      ),
                    );
                  },
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
