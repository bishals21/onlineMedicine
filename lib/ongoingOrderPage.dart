import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:online_medicine/appText.dart';
import 'package:online_medicine/model/orderItem.dart';
import 'package:http/http.dart' as http;
import 'package:online_medicine/myOrderDetail.dart';

class OnGoingOrderPage extends StatefulWidget {
  final String accessToken;
  OnGoingOrderPage({Key key, @required this.accessToken}) : super(key: key);
  @override
  _OnGoingOrderPageState createState() => _OnGoingOrderPageState();
}

class _OnGoingOrderPageState extends State<OnGoingOrderPage>
    with AutomaticKeepAliveClientMixin<OnGoingOrderPage> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  List<OrderItem> orderList = new List();
  bool isEmptyCheck = false;

  Future<bool> checkConnection() async {
    bool connected = false;
    try {
      final result = await InternetAddress.lookup('google.com');
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        print('connected');
        connected = true;
      }
    } on SocketException catch (_) {
      print('not connected');
      connected = false;
    }

    return connected;
  }

  getOrderList() async {
    bool isConnected = await checkConnection();
    if (isConnected) {
      http.get(AppText.baseURl + "order?_format=json&type=ONGOING", headers: {
        "api-key": AppText.apiKey,
        "access-token": widget.accessToken,
      }).then((response) {
        print("Response Body:${response.body}");
        print("accesstoken:    " + widget.accessToken);
        if (response.statusCode == 200) {
          List<OrderItem> listOfItem = new List();
          var responseBody = json.decode(response.body);
          if (responseBody['status'] == true) {
            var orders = responseBody['data']['orders'];
            for (int i = 0; i < orders.length; i++) {
              var orderItem = orders[i];
              List<Item> itemList = new List();
              List<Prescription> prescriptionList = new List();
              for (int j = 0; j < orderItem['items'].length; j++) {
                var item = orderItem['items'][j];
                itemList.add(new Item(
                    item['id'],
                    item['name'],
                    item['rate'].toString(),
                    item['appliedRate'].toString(),
                    item['quantity'],
                    item['total'].toString()));
              }

              for (int k = 0; k < orderItem['prescriptions'].length; k++) {
                var item = orderItem['prescriptions'][k];
                prescriptionList.add(new Prescription(
                  item['id'],
                  item['file'],
                ));
              }

              listOfItem.add(new OrderItem(
                  orderItem['id'],
                  orderItem['tracking_number'],
                  new Address(
                      orderItem['delivery_address']['tole'],
                      orderItem['delivery_address']['city'],
                      orderItem['delivery_address']['address'],
                      orderItem['delivery_address']['latitude'],
                      orderItem['delivery_address']['longitude']),
                  orderItem['status'],
                  prescriptionList,
                  orderItem['createdOn'],
                  orderItem['updateOn'],
                  itemList));
            }

            setState(() {
              orderList = listOfItem;
              isEmptyCheck = true;
            });
          } else {
            _showSnackBar(responseBody["message"]);
          }
        }
      });
    } else {
      _showSnackBar("No Internet Connection");
    }
  }

  void _showSnackBar(String text) {
    _scaffoldKey.currentState.showSnackBar(new SnackBar(
      content: new Text(text),
    ));
  }

  @override
  void initState() {
    getOrderList();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      key: _scaffoldKey,
      body: orderList.isNotEmpty
          ? new ListView.builder(
              padding: new EdgeInsets.all(8.0),
              itemBuilder: (context, index) {
                return new ListTile(
                  leading: new CircleAvatar(
                    backgroundColor: const Color(0xF29E30BF),
                    child: new Icon(
                      Icons.receipt,
                      color: Colors.white,
                    ),
                  ),
                  title: new Text(orderList[index].trackingNumber),
                  subtitle: new Text(orderList[index].updatedOn),
                  onTap: () => Navigator.push(
                        context,
                        new MaterialPageRoute(
                            builder: (context) => new MyOrderDetailPage(
                                  orderItem: orderList[index],
                                )),
                      ),
                );
              },
              itemCount: orderList.length,
            )
          : isEmptyCheck == false
              ? new Center(
                  child: new CircularProgressIndicator(
                    valueColor:
                        new AlwaysStoppedAnimation(const Color(0xF29E30BF)),
                  ),
                )
              : Center(
                  child: new Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      new Image.asset(
                        "assets/empty.png",
                        height: 100.0,
                        width: 100.0,
                      ),
                      new Text(
                        "No item",
                        style: new TextStyle(
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ],
                  ),
                ),
    );
  }

  // TODO: implement wantKeepAlive
  @override
  bool get wantKeepAlive => true;
}
