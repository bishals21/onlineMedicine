import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:http_parser/http_parser.dart';
import 'package:image_picker/image_picker.dart';
import 'package:map_view/map_view.dart';
import 'package:online_medicine/appText.dart';
import 'package:online_medicine/appbar/gradientAppBarWithBack.dart';
import 'package:online_medicine/model/onlineMedicineItem.dart';
import 'package:online_medicine/model/userAccessToken.dart';
import 'package:online_medicine/stateContainer/stateContainer.dart';

class OrderModel {
  OrderModel(this.id, this.quantity);
  int id;
  int quantity;

  Map<String, dynamic> toJson() {
    var map = new Map<String, dynamic>();
    map["id"] = id;
    map["quantity"] = quantity;
    return map;
  }

  OrderModel.fromJson(Map<String, dynamic> map)
      : this(
          map["id"],
          map["quantity"],
        );
}

class CheckoutPage extends StatefulWidget {
  @override
  _CheckoutPageState createState() => _CheckoutPageState();
}

class _CheckoutPageState extends State<CheckoutPage> {
  GlobalKey<FormState> _formKey = new GlobalKey();
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  var mapView = new MapView();
  Marker marker;

  int _currentStep = 0;
  List<File> imageList = new List();

  String addressTole = "", addressCity = "", addressDelivery = "";
  Location addressLocation;

  ThemeData buildTheme() {
    final ThemeData base = ThemeData();
    return base.copyWith(
      primaryColor: Color(0xFF9E30BF),
    );
  }

  Future getImageFromCamera() async {
    var image = await ImagePicker.pickImage(source: ImageSource.camera);

    setState(() {
      if (image != null) {
        imageList.add(image);
      }
    });
  }

  Future getImageFromGallery() async {
    var image = await ImagePicker.pickImage(source: ImageSource.gallery);

    setState(() {
      if (image != null) {
        imageList.add(image);
      }
    });
  }

  @override
  void initState() {
    MapView.setApiKey(AppText.googleApiKey);
    super.initState();
  }

  void _showSnackBar(String text) {
    _scaffoldKey.currentState.showSnackBar(new SnackBar(
      content: new Text(text),
    ));
  }

  String _validateField(String value) {
    if (value.length == 0) {
      return "Please fill out this field";
    }

    return null;
  }

  Future<bool> checkConnection() async {
    bool connected = false;
    try {
      final result = await InternetAddress.lookup('google.com');
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        print('connected');
        connected = true;
      }
    } on SocketException catch (_) {
      print('not connected');
      connected = false;
    }

    return connected;
  }

  @override
  Widget build(BuildContext context) {
    var container = StateContainer.of(context);
    List<OnlineMedicineItem> cartList = container.cartItemList;
    UserAccessToken userAccessToken = container.userAccessToken;

    double totalPrice = 0.0;
    bool isPrescriptionRequired = false;
    cartList.forEach((item) {
      totalPrice = totalPrice +
          (double.parse(item.applicableRate) * item.quantityInCart);
      isPrescriptionRequired = item.isPrescriptionRequired;
      //print(isPrescriptionRequired);
    });

    var uploadDescriptionContent = new Column(
      children: <Widget>[
        new Container(
          padding: new EdgeInsets.all(8.0),
          child: new Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: <Widget>[
              new Column(
                children: <Widget>[
                  new GestureDetector(
                    onTap: getImageFromCamera,
                    child: new Image.asset(
                      "assets/camera.png",
                      height: 80.0,
                      width: 80.0,
                    ),
                  ),
                  new Text(
                    "Camera",
                    style: new TextStyle(
                      fontWeight: FontWeight.bold,
                    ),
                  )
                ],
              ),
              new Column(
                children: <Widget>[
                  new GestureDetector(
                    onTap: getImageFromGallery,
                    child: new Image.asset(
                      "assets/gallery.png",
                      height: 80.0,
                      width: 80.0,
                    ),
                  ),
                  new Text(
                    "Gallery",
                    style: new TextStyle(
                      fontWeight: FontWeight.bold,
                    ),
                  )
                ],
              )
            ],
          ),
        ),
        imageList.isNotEmpty
            ? new Container(
                child: new GridView.count(
                  padding: new EdgeInsets.all(8.0),
                  crossAxisCount: 3,
                  crossAxisSpacing: 8.0,
                  mainAxisSpacing: 8.0,
                  shrinkWrap: true,
                  children: imageList.map((item) {
                    return new Container(
                      height: 100.0,
                      width: 100.0,
                      padding: new EdgeInsets.all(4.0),
                      decoration: new BoxDecoration(
                        border: new Border.all(
                          color: Colors.grey,
                          width: 0.7,
                        ),
                        borderRadius:
                            BorderRadius.all(new Radius.circular(8.0)),
                      ),
                      child: Stack(
                        children: <Widget>[
                          new SizedBox.expand(
                            child: new Image.file(
                              item,
                              fit: BoxFit.cover,
                            ),
                          ),
                          new Positioned(
                            top: 0.0,
                            right: 0.0,
                            child: new GestureDetector(
                              onTap: () {
                                setState(() {
                                  imageList.remove(item);
                                });
                              },
                              child: new Icon(
                                Icons.remove_circle,
                                size: 24.0,
                                color: Color(0xFF9E30BF),
                              ),
                            ),
                          ),
                        ],
                      ),
                    );
                  }).toList(),
                ),
              )
            : new Container(
                margin: new EdgeInsets.only(top: 24.0),
                padding: new EdgeInsets.all(8.0),
                decoration: new BoxDecoration(
                    border: new Border.all(
                  width: 1.5,
                  color: const Color(0xF2262BA0),
                )),
                child: new Text(
                  "Image not added",
                  textAlign: TextAlign.center,
                  style: new TextStyle(
                    fontSize: 18.0,
                    color: const Color(0xF2262BA0),
                    //fontWeight: FontWeight.bold,
                  ),
                ),
              ),
      ],
    );

    var addressContent = new Form(
      key: _formKey,
      child: new Column(
        children: <Widget>[
          new TextFormField(
            keyboardType: TextInputType.text,
            decoration: new InputDecoration(
              labelText: "Tole",
              border: new OutlineInputBorder(),
            ),
            validator: this._validateField,
            onSaved: (String value) {
              this.addressTole = value;
            },
          ),
          new Padding(
            padding: new EdgeInsets.only(top: 16.0),
          ),
          new TextFormField(
            keyboardType: TextInputType.text,
            decoration: new InputDecoration(
              labelText: "City",
              border: new OutlineInputBorder(),
            ),
            validator: this._validateField,
            onSaved: (String value) {
              this.addressCity = value;
            },
          ),
          new Padding(
            padding: new EdgeInsets.only(top: 16.0),
          ),
          new TextFormField(
            keyboardType: TextInputType.text,
            decoration: new InputDecoration(
              labelText: "Your delivery address",
              border: new OutlineInputBorder(),
            ),
            validator: this._validateField,
            onSaved: (String value) {
              this.addressDelivery = value;
            },
          ),
          new Padding(
            padding: new EdgeInsets.only(top: 16.0),
          ),
          new InkWell(
            child: new Container(
              width: 220.0,
              child: new Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  new Text(
                    "Give your location",
                    style: new TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: 16.0,
                    ),
                  ),
                  new Image.asset(
                    "assets/mapIcon.png",
                    height: 50.0,
                    width: 50.0,
                  ),
                ],
              ),
            ),
            onTap: () {
              mapView.show(
                new MapOptions(
                  showUserLocation: true,
                  showMyLocationButton: true,
                  showCompassButton: true,
                  initialCameraPosition:
                      CameraPosition(new Location(27.7172, 85.3240), 17.0),
                ),
                toolbarActions: [new ToolbarAction("Done", 1)],
              );
              mapView.onMapTapped.listen((location) {
                if (marker == null) {
                  marker = new Marker("yourlocation", "Your Location",
                      location.latitude, location.longitude);
                  mapView.addMarker(marker);
                  addressLocation = location;
                } else {
                  mapView.removeMarker(marker);
                  marker = new Marker("yourlocation", "Your Location",
                      location.latitude, location.longitude);
                  mapView.addMarker(marker);
                  addressLocation = location;
                }
              });

              mapView.onLocationUpdated
                  .listen((location) => addressLocation = location);
              mapView.onToolbarAction.listen((id) {
                if (id == 1) {
                  mapView.dismiss();
                  print(addressLocation.toString());
                }
              });
            },
          ),
        ],
      ),
    );

    var reviewContent = new Column(
      children: <Widget>[
        new Table(
          border: new TableBorder(
            top: BorderSide(
              width: 2.0,
              color: Color(0xFF9E30BF),
            ),
          ),
          children: [
            new TableRow(children: [
              Padding(
                padding: const EdgeInsets.all(4.0),
                child: new Text(
                  "Item",
                  textAlign: TextAlign.center,
                  style: new TextStyle(
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(4.0),
                child: new Text(
                  "Rate",
                  textAlign: TextAlign.center,
                  style: new TextStyle(
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(4.0),
                child: new Text(
                  "Quantity",
                  textAlign: TextAlign.center,
                  style: new TextStyle(
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              //   Padding(
              //     padding: const EdgeInsets.all(4.0),
              //     child: new Text(
              //       "Offer",
              //       textAlign: TextAlign.center,
              //       style: new TextStyle(
              //         fontWeight: FontWeight.bold,
              //       ),
              //     ),
              //   ),
              Padding(
                padding: const EdgeInsets.all(4.0),
                child: new Text(
                  "Total",
                  textAlign: TextAlign.center,
                  style: new TextStyle(
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
            ]),
          ],
        ),
        new Table(
            border: new TableBorder(
              top: BorderSide(
                width: 2.0,
                color: Color(0xFF9E30BF),
              ),
              bottom: BorderSide(
                width: 2.0,
                color: Color(0xFF9E30BF),
              ),
              horizontalInside: BorderSide(
                width: 2.0,
                color: Color(0xFF9E30BF),
              ),
            ),
            children: cartList.map((item) {
              return new TableRow(children: [
                Padding(
                  padding: const EdgeInsets.all(4.0),
                  child: new Text(
                    item.name,
                    maxLines: 2,
                    overflow: TextOverflow.ellipsis,
                    textAlign: TextAlign.center,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(4.0),
                  child: new Text(
                    item.applicableRate,
                    textAlign: TextAlign.center,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(4.0),
                  child: new Text(
                    item.quantityInCart.toString(),
                    textAlign: TextAlign.center,
                  ),
                ),
                // Padding(
                //   padding: const EdgeInsets.all(4.0),
                //   child: new Text(
                //     item.offer,
                //     textAlign: TextAlign.center,
                //   ),
                // ),
                Padding(
                  padding: const EdgeInsets.all(4.0),
                  child: new Text(
                    (double.parse(item.applicableRate) * item.quantityInCart)
                        .toStringAsFixed(2),
                    textAlign: TextAlign.center,
                  ),
                ),
              ]);
            }).toList()),
        new Table(
          children: [
            new TableRow(children: [
              Padding(
                padding: const EdgeInsets.all(4.0),
                child: new Text(
                  "",
                  textAlign: TextAlign.center,
                  style: new TextStyle(
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(4.0),
                child: new Text(
                  "",
                  textAlign: TextAlign.center,
                  style: new TextStyle(
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              //   Padding(
              //     padding: const EdgeInsets.all(4.0),
              //     child: new Text(
              //       "",
              //       textAlign: TextAlign.center,
              //       style: new TextStyle(
              //         fontWeight: FontWeight.bold,
              //       ),
              //     ),
              //   ),
              Padding(
                padding: const EdgeInsets.all(4.0),
                child: new Text(
                  "Total",
                  textAlign: TextAlign.center,
                  style: new TextStyle(
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(4.0),
                child: new Text(
                  totalPrice.toStringAsFixed(2),
                  textAlign: TextAlign.center,
                  style: new TextStyle(
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
            ]),
          ],
        ),
      ],
    );

    _postOrder() async {
      bool isConnected = await checkConnection();
      if (isConnected) {
        List<OrderModel> orderList = new List();
        cartList.forEach((item) {
          orderList.add(new OrderModel(item.id, item.quantityInCart));
        });

        http.MultipartRequest request = new http.MultipartRequest(
            'POST', Uri.parse(AppText.baseURl + "order"));

        request.headers['api-key'] = AppText.apiKey;
        request.headers['access-token'] = userAccessToken.accessToken;

        if (imageList.isNotEmpty) {
          for (int i = 0; i < imageList.length; i++) {
            var file = imageList[i];
            List<int> imageBytes = await file.readAsBytes();
            request.files.add(
              new http.MultipartFile.fromBytes(
                'prescription[$i]',
                imageBytes,
                contentType: new MediaType('image', 'jpeg'),
                filename: file.path,
              ),
            );
          }
        }

        request.fields['tole'] = addressTole;
        request.fields['city'] = addressCity;
        request.fields['address'] = addressDelivery;
        request.fields['latitude'] = addressLocation.latitude.toString();
        request.fields['longitude'] = addressLocation.longitude.toString();
        request.fields['items'] = json.encode(orderList);

        http.StreamedResponse r = await request.send();
        r.stream.transform(utf8.decoder).listen((value) {
          var responseData = json.decode(value);
          print(responseData);
          if (responseData['status'] == true) {
            _showSnackBar("Ordered Successfully");
            new Future.delayed(new Duration(seconds: 2));
            container.updateCartList([]);
            Navigator.popUntil(
                context, ModalRoute.withName(Navigator.defaultRouteName));
          }
        });
      }
    }

    return new Theme(
      data: buildTheme(),
      child: new Scaffold(
        key: _scaffoldKey,
        body: new Column(
          children: <Widget>[
            new GradientAppBarWithBack("Checkout"),
            new Expanded(
              child: new Stepper(
                steps: <Step>[
                  new Step(
                    title: new Text(
                      "Upload \n Prescription",
                      textAlign: TextAlign.center,
                    ),
                    //state: StepState.disabled,
                    content: uploadDescriptionContent,
                    isActive: _currentStep == 0,
                    state: _currentStep > 0
                        ? StepState.complete
                        : StepState.indexed,
                  ),
                  new Step(
                    title: new Text("Address"),
                    content: addressContent,
                    isActive: _currentStep == 1,
                    state: _currentStep > 1
                        ? StepState.complete
                        : StepState.indexed,
                  ),
                  new Step(
                    title: new Text("Review"),
                    content: reviewContent,
                    isActive: _currentStep == 2,
                  ),
                ],
                currentStep: _currentStep,
                type: StepperType.horizontal,
                onStepContinue: () {
                  if (_currentStep < 2) {
                    if (_currentStep == 1) {
                      if (this._formKey.currentState.validate()) {
                        if (addressLocation != null) {
                          _formKey.currentState.save();
                          setState(() => _currentStep = _currentStep + 1);
                        } else {
                          _showSnackBar("Please give location in map");
                        }
                      }
                    }
                    if (_currentStep == 0) {
                      if (isPrescriptionRequired) {
                        setState(() => imageList.isNotEmpty
                            ? _currentStep = _currentStep + 1
                            : null);
                      } else {
                        setState(() => _currentStep = _currentStep + 1);
                      }
                    }
                  } else {
                    _postOrder();
                  }
                },
                onStepCancel: _currentStep > 0
                    ? () => setState(() => _currentStep = _currentStep - 1)
                    : null,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
