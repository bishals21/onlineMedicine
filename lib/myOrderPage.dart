import 'package:flutter/material.dart';
import 'package:online_medicine/appbar/gradientWithCart.dart';
import 'package:online_medicine/model/onlineMedicineItem.dart';
import 'package:online_medicine/ongoingOrderPage.dart';
import 'package:online_medicine/pastOrderPage.dart';
import 'package:online_medicine/stateContainer/stateContainer.dart';

class MyOrderPage extends StatefulWidget {
  @override
  _MyOrderPageState createState() => _MyOrderPageState();
}

class _MyOrderPageState extends State<MyOrderPage> {
  @override
  Widget build(BuildContext context) {
    final container = StateContainer.of(context);
    List<OnlineMedicineItem> cartList = container.cartItemList;
    return new DefaultTabController(
      length: 2,
      child: new Scaffold(
        body: new Column(
          children: <Widget>[
            new GradientAppBarWithCart("My Order", cartList, false),
            new Container(
              decoration: new BoxDecoration(
                gradient: new LinearGradient(
                    colors: [const Color(0xFF9E30BF), const Color(0xFF262BA0)],
                    begin: const FractionalOffset(0.0, 0.0),
                    end: const FractionalOffset(1.0, 0.0),
                    stops: [0.0, 1.0],
                    tileMode: TileMode.clamp),
              ),
              child: new TabBar(
                indicatorColor: Colors.white,
                tabs: <Widget>[
                  new Tab(
                    child: new Text("ONGOING"),
                  ),
                  new Tab(
                    child: new Text("PAST"),
                  ),
                ],
              ),
            ),
            new Expanded(
              child: new TabBarView(
                children: <Widget>[
                  container.userAccessToken != null
                      ? new OnGoingOrderPage(
                          accessToken: container.userAccessToken.accessToken,
                        )
                      : new Center(
                          child: new Text("Please login to see your order"),
                        ),
                  container.userAccessToken != null
                      ? new PastOrderPage(
                          accessToken: container.userAccessToken.accessToken,
                        )
                      : new Center(
                          child: new Text("Please login to see your order"),
                        ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
