import 'package:flutter/material.dart';
import 'package:online_medicine/appText.dart';
import 'package:online_medicine/appbar/gradientWithCart.dart';
import 'package:online_medicine/imageViewer.dart';
import 'package:online_medicine/model/orderItem.dart';
import 'package:online_medicine/stateContainer/stateContainer.dart';

class MyOrderDetailPage extends StatefulWidget {
  final OrderItem orderItem;
  MyOrderDetailPage({Key key, @required this.orderItem}) : super(key: key);
  @override
  _MyOrderDetailPageState createState() => _MyOrderDetailPageState();
}

class _MyOrderDetailPageState extends State<MyOrderDetailPage> {
  OrderItem orderItem;

  @override
  void initState() {
    orderItem = widget.orderItem;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final container = StateContainer.of(context);
    double totalPrice = 0.0;
    return new Scaffold(
      body: new Column(
        children: <Widget>[
          new GradientAppBarWithCart(
              orderItem.trackingNumber, container.cartItemList, false),
          new Expanded(
            child: new ListView(
              padding: new EdgeInsets.all(8.0),
              children: <Widget>[
                new Container(
                  padding: new EdgeInsets.all(8.0),
                  decoration: new BoxDecoration(
                    border: new Border.all(
                      color: const Color(0xFF9E30BF),
                      width: 2.0,
                    ),
                  ),
                  child: new Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      new Text(
                        "DELIVERY ADDRESS:",
                        style: new TextStyle(
                          fontSize: 16.0,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      new Padding(
                        padding: new EdgeInsets.only(top: 12.0),
                      ),
                      new Table(
                        columnWidths: const <int, TableColumnWidth>{
                          0: const FixedColumnWidth(150.0),
                        },
                        children: [
                          new TableRow(children: [
                            new Text(
                              "Tole:",
                              style: new TextStyle(
                                  color: Colors.black,
                                  fontWeight: FontWeight.bold,
                                  fontSize: 15.0),
                            ),
                            new Text(
                              orderItem.adderss.tole,
                              style: new TextStyle(fontSize: 15.0),
                            ),
                          ]),
                          new TableRow(children: [
                            new Text(
                              "City:",
                              style: new TextStyle(
                                  color: Colors.black,
                                  fontWeight: FontWeight.bold,
                                  fontSize: 15.0),
                            ),
                            new Text(
                              orderItem.adderss.city,
                              style: new TextStyle(fontSize: 15.0),
                            ),
                          ]),
                          new TableRow(children: [
                            new Text(
                              "Address:",
                              style: new TextStyle(
                                  color: Colors.black,
                                  fontWeight: FontWeight.bold,
                                  fontSize: 15.0),
                            ),
                            new Text(
                              orderItem.adderss.address,
                              style: new TextStyle(fontSize: 15.0),
                            ),
                          ]),
                        ],
                      ),
                    ],
                  ),
                ),
                new Padding(
                  padding: new EdgeInsets.only(top: 12.0),
                ),
                new Container(
                  padding: new EdgeInsets.all(8.0),
                  decoration: new BoxDecoration(
                    border: new Border.all(
                      color: const Color(0xFF9E30BF),
                      width: 2.0,
                    ),
                  ),
                  child: new Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      new Text(
                        "PRESCRIPTION:",
                        style: new TextStyle(
                          fontSize: 16.0,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      new Padding(
                        padding: new EdgeInsets.only(top: 12.0),
                      ),
                      orderItem.prescription.isNotEmpty
                          ? new Container(
                              child: new GridView.count(
                                padding: new EdgeInsets.all(8.0),
                                crossAxisCount: 3,
                                crossAxisSpacing: 8.0,
                                mainAxisSpacing: 8.0,
                                shrinkWrap: true,
                                physics: new ScrollPhysics(
                                    parent: ClampingScrollPhysics()),
                                children: orderItem.prescription.map((item) {
                                  return new GestureDetector(
                                    onTap: () {
                                      Navigator.push(
                                          context,
                                          MaterialPageRoute(
                                            builder: (context) =>
                                                new HeroPhotoViewWrapper(
                                                  file: AppText.prefixImage +
                                                      item.file,
                                                  tag: item.file +
                                                      item.id.toString(),
                                                ),
                                            fullscreenDialog: true,
                                          ));
                                    },
                                    child: new Container(
                                      height: 100.0,
                                      width: 100.0,
                                      padding: new EdgeInsets.all(4.0),
                                      decoration: new BoxDecoration(
                                        border: new Border.all(
                                          color: Colors.grey,
                                          width: 0.7,
                                        ),
                                        borderRadius: BorderRadius.all(
                                            new Radius.circular(8.0)),
                                      ),
                                      child: new Hero(
                                        tag: item.file + item.id.toString(),
                                        child: new Image.network(
                                          AppText.prefixImage + item.file,
                                          fit: BoxFit.cover,
                                        ),
                                      ),
                                    ),
                                  );
                                }).toList(),
                              ),
                            )
                          : new Text("Not added")
                    ],
                  ),
                ),
                new Padding(
                  padding: new EdgeInsets.only(top: 12.0),
                ),
                new Container(
                  padding: new EdgeInsets.all(8.0),
                  decoration: new BoxDecoration(
                    border: new Border.all(
                      color: const Color(0xFF9E30BF),
                      width: 2.0,
                    ),
                  ),
                  child: new Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      new Text(
                        "ITEMS:",
                        style: new TextStyle(
                          fontSize: 16.0,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      new Padding(
                        padding: new EdgeInsets.only(top: 12.0),
                      ),
                      orderItem.itemList.isNotEmpty
                          ? new Column(
                              children: <Widget>[
                                new Table(
                                  children: [
                                    new TableRow(children: [
                                      Padding(
                                        padding: const EdgeInsets.all(4.0),
                                        child: new Text(
                                          "Item",
                                          textAlign: TextAlign.center,
                                          style: new TextStyle(
                                            fontWeight: FontWeight.bold,
                                          ),
                                        ),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.all(4.0),
                                        child: new Text(
                                          "Rate",
                                          textAlign: TextAlign.center,
                                          style: new TextStyle(
                                            fontWeight: FontWeight.bold,
                                          ),
                                        ),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.all(4.0),
                                        child: new Text(
                                          "Quantity",
                                          textAlign: TextAlign.center,
                                          style: new TextStyle(
                                            fontWeight: FontWeight.bold,
                                          ),
                                        ),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.all(4.0),
                                        child: new Text(
                                          "Total",
                                          textAlign: TextAlign.center,
                                          style: new TextStyle(
                                            fontWeight: FontWeight.bold,
                                          ),
                                        ),
                                      ),
                                    ]),
                                  ],
                                ),
                                new Table(
                                    border: new TableBorder(
                                      top: BorderSide(
                                        width: 2.0,
                                        color: Color(0xFF9E30BF),
                                      ),
                                      bottom: BorderSide(
                                        width: 2.0,
                                        color: Color(0xFF9E30BF),
                                      ),
                                      horizontalInside: BorderSide(
                                        width: 2.0,
                                        color: Color(0xFF9E30BF),
                                      ),
                                    ),
                                    children: orderItem.itemList.map((item) {
                                      totalPrice =
                                          totalPrice + double.parse(item.total);
                                      return new TableRow(children: [
                                        Padding(
                                          padding: const EdgeInsets.all(4.0),
                                          child: new Text(
                                            item.name,
                                            maxLines: 2,
                                            overflow: TextOverflow.ellipsis,
                                            textAlign: TextAlign.center,
                                          ),
                                        ),
                                        Padding(
                                          padding: const EdgeInsets.all(4.0),
                                          child: new Text(
                                            item.appliedRate,
                                            textAlign: TextAlign.center,
                                          ),
                                        ),
                                        Padding(
                                          padding: const EdgeInsets.all(4.0),
                                          child: new Text(
                                            item.quantity.toString(),
                                            textAlign: TextAlign.center,
                                          ),
                                        ),
                                        Padding(
                                          padding: const EdgeInsets.all(4.0),
                                          child: new Text(
                                            item.total,
                                            textAlign: TextAlign.center,
                                          ),
                                        ),
                                      ]);
                                    }).toList()),
                                new Table(
                                  children: [
                                    new TableRow(children: [
                                      Padding(
                                        padding: const EdgeInsets.all(4.0),
                                        child: new Text(
                                          "",
                                          textAlign: TextAlign.center,
                                          style: new TextStyle(
                                            fontWeight: FontWeight.bold,
                                          ),
                                        ),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.all(4.0),
                                        child: new Text(
                                          "",
                                          textAlign: TextAlign.center,
                                          style: new TextStyle(
                                            fontWeight: FontWeight.bold,
                                          ),
                                        ),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.all(4.0),
                                        child: new Text(
                                          "Total",
                                          textAlign: TextAlign.center,
                                          style: new TextStyle(
                                            fontWeight: FontWeight.bold,
                                          ),
                                        ),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.all(4.0),
                                        child: new Text(
                                          totalPrice.toStringAsFixed(2),
                                          textAlign: TextAlign.center,
                                          style: new TextStyle(
                                            fontWeight: FontWeight.bold,
                                          ),
                                        ),
                                      ),
                                    ]),
                                  ],
                                ),
                              ],
                            )
                          : new Text("No item")
                    ],
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
