import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:online_medicine/appText.dart';
import 'package:online_medicine/cartPage.dart';
import 'package:online_medicine/categoryPage.dart';
import 'package:online_medicine/loginPage.dart';
import 'package:online_medicine/medicineDetailEndPage.dart';
import 'package:online_medicine/model/onlineMedicineCategory.dart';
import 'package:online_medicine/model/onlineMedicineItem.dart';
import 'package:online_medicine/model/userAccessToken.dart';
import 'package:online_medicine/myOrderPage.dart';
import 'package:online_medicine/specialOfferPage.dart';
import 'package:online_medicine/stateContainer/stateContainer.dart';
import 'package:online_medicine/uploadPrescription.dart';
import 'package:shared_preferences/shared_preferences.dart';

class OnlineMedicinePage extends StatefulWidget {
  @override
  _OnlineMedicinePageState createState() => _OnlineMedicinePageState();
}

class _OnlineMedicinePageState extends State<OnlineMedicinePage> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  Future<SharedPreferences> _prefs = SharedPreferences.getInstance();

  int cartCounter = 0;
  List<OnlineMedicineCategory> categoryList = new List();
  List<OnlineMedicineItem> trendingList = new List();

  Future<bool> checkConnection() async {
    bool connected = false;
    try {
      final result = await InternetAddress.lookup('google.com');
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        print('connected');
        connected = true;
      }
    } on SocketException catch (_) {
      print('not connected');
      connected = false;
    }

    return connected;
  }

  getCategory() async {
    bool isConnected = await checkConnection();
    if (isConnected) {
      http.get(AppText.baseURl + "categories?_format=json", headers: {
        "api-key": AppText.apiKey,
      }).then((response) {
        print("Response Body:${response.body}");
        if (response.statusCode == 200) {
          List<OnlineMedicineCategory> onlineMedicineCategory = new List();
          var responseBody = json.decode(response.body);
          if (responseBody['status'] == true) {
            var categories = responseBody['data']['categories'];
            for (int i = 0; i < categories.length; i++) {
              onlineMedicineCategory.add(new OnlineMedicineCategory(
                  categories[i]['id'],
                  categories[i]['name'],
                  categories[i]['slug'],
                  categories[i]['image'],
                  categories[i]['description']));
            }

            //categoryList.removeWhere(test)

            setState(() {
              categoryList = onlineMedicineCategory;
            });
          } else {
            _showSnackBar(responseBody["message"]);
          }
        }
      });
    } else {
      _showSnackBar("No Internet Connection");
    }
  }

  getTrending() async {
    List<OnlineMedicineItem> listOfItem = new List();
    final container = StateContainer.of(context);
    bool isConnected = await checkConnection();
    if (isConnected) {
      http.get(AppText.baseURl + "medicine/trending?_format=json&limit=15",
          headers: {
            "api-key": AppText.apiKey,
          }).then((response) {
        print("Response Body:${response.body}");
        if (response.statusCode == 200) {
          var responseBody = json.decode(response.body);

          var medicines = responseBody["data"]["medicines"];

          for (var item in medicines) {
            listOfItem.add(new OnlineMedicineItem(
              item["id"],
              item["name"],
              item["image"],
              item["description"],
              item["company"],
              item["companyId"],
              item["category"],
              item["categoryId"].toString(),
              item["composition"],
              item["rate"].toString(),
              item["offer"].toString(),
              item["applicableRate"].toString(),
              item["rateDescription"],
              item["isPrescriptionRequired"],
              0,
            ));
          }

          for (int i = 0; i < container.cartItemList.length; i++) {
            for (int j = 0; j < listOfItem.length; j++) {
              if (container.cartItemList[i].id == listOfItem[j].id) {
                OnlineMedicineItem item = listOfItem[j];
                item.updateAddToCart(container.cartItemList[i].quantityInCart);
                listOfItem.removeAt(j);
                listOfItem.insert(j, item);
              }
            }
          }

          setState(() {
            trendingList = listOfItem;
          });
        } else {
          print(response);
        }
      });
    } else {
      _showSnackBar("No Internet Connection");
    }
  }

  void _showSnackBar(String text) {
    _scaffoldKey.currentState.showSnackBar(new SnackBar(
      content: new Text(text),
    ));
  }

  Future<List<OnlineMedicineItem>> _getCartList() async {
    List<OnlineMedicineItem> cartItemList = new List();
    final SharedPreferences prefs = await _prefs;
    String cartListString = prefs.getString('OMcartList') ?? null;
    if (cartListString != null) {
      json.decode(cartListString).forEach(
          (map) => cartItemList.add(new OnlineMedicineItem.fromJson(map)));
    }
    return cartItemList;
  }

  Future<UserAccessToken> _getUserAccessToken() async {
    UserAccessToken accessToken;
    final SharedPreferences prefs = await _prefs;
    String tokenString = prefs.getString('OMUser') ?? null;
    if (tokenString != null) {
      if (tokenString != "null") {
        accessToken = new UserAccessToken.fromJson(json.decode(tokenString));
      }
    }

    return accessToken;
  }

  @override
  void initState() {
    new Future.delayed(Duration.zero, () {
      final container = StateContainer.of(context);
      _getCartList().then((List<OnlineMedicineItem> list) {
        container.updateCartList(list);
      });
      _getUserAccessToken().then((UserAccessToken userAccessToken) {
        container.updateUserAccessToken(userAccessToken);
      });

      getCategory();
      getTrending();
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final double statusBarHeight = MediaQuery.of(context).padding.top;
    final container = StateContainer.of(context);

    return new Scaffold(
      key: _scaffoldKey,
      drawer: new Drawer(
        child: new ListView(
          padding: new EdgeInsets.all(0.0),
          children: <Widget>[
            new Container(
              height: 200.0,
              decoration: new BoxDecoration(
                gradient: new LinearGradient(
                    colors: [const Color(0xFF9E30BF), const Color(0xFF262BA0)],
                    begin: const FractionalOffset(0.0, 0.0),
                    end: const FractionalOffset(1.0, 0.0),
                    stops: [0.0, 1.0],
                    tileMode: TileMode.clamp),
              ),
              child: new Center(
                child: new ClipOval(
                  child: new Container(
                    height: 90.0,
                    width: 90.0,
                    color: Colors.white,
                  ),
                ),
              ),
            ),
            new ListTile(
              leading: new Image.asset(
                "assets/myOrder.png",
                width: 30.0,
                height: 30.0,
              ),
              title: new Text("My Order"),
              onTap: () => Navigator.push(
                    context,
                    new MaterialPageRoute(
                        builder: (context) => new MyOrderPage()),
                  ),
            ),
            new ListTile(
              leading: new Image.asset(
                "assets/offer.png",
                width: 30.0,
                height: 30.0,
              ),
              title: new Text("Offer"),
              onTap: () => Navigator.push(
                    context,
                    new MaterialPageRoute(
                        builder: (context) => new SpecialOfferPage()),
                  ),
            ),
            new ListTile(
              leading: new Image.asset(
                "assets/about.png",
                width: 30.0,
                height: 30.0,
              ),
              title: new Text("About Us"),
              onTap: () {},
            ),
            container.userAccessToken == null
                ? new ListTile(
                    leading: new Image.asset(
                      "assets/userLogin.png",
                      width: 30.0,
                      height: 30.0,
                    ),
                    title: new Text("Login"),
                    onTap: () => Navigator.push(
                        context,
                        new MaterialPageRoute(
                            builder: (context) => new LoginPage())),
                  )
                : new ListTile(
                    leading: new Image.asset(
                      "assets/userLogout.png",
                      width: 30.0,
                      height: 30.0,
                    ),
                    title: new Text("Logout"),
                    onTap: () {
                      showDialog(
                          context: context,
                          builder: (context) {
                            return new AlertDialog(
                              title: new Text("Logout"),
                              content: new Text("Do you want to logout ?"),
                              actions: <Widget>[
                                new FlatButton(
                                  child: new Text(
                                    "Cancel",
                                    style: new TextStyle(
                                      color: Colors.red,
                                    ),
                                  ),
                                  onPressed: () {
                                    Navigator.pop(context);
                                  },
                                ),
                                new FlatButton(
                                  child: new Text("OK"),
                                  onPressed: () {
                                    Navigator.pop(context);
                                    container.updateUserAccessToken(null);
                                  },
                                )
                              ],
                            );
                          });
                    },
                  ),
          ],
        ),
      ),
      body: new Column(
        children: <Widget>[
          new Container(
            padding: new EdgeInsets.only(top: statusBarHeight),
            height: statusBarHeight + 66.0,
            child: new Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                new IconButton(
                  icon: new Icon(
                    Icons.menu,
                    color: Colors.white,
                    size: 28.0,
                  ),
                  onPressed: () => _scaffoldKey.currentState.openDrawer(),
                ),
                new Container(
                  padding: const EdgeInsets.symmetric(horizontal: 25.0),
                  child: new Text(
                    "ONLINE MEDICINE",
                    style: const TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.w600,
                        fontSize: 18.0),
                  ),
                ),
                new Stack(
                  children: <Widget>[
                    new IconButton(
                      icon: new Icon(
                        Icons.shopping_cart,
                        color: Colors.white,
                        size: 24.0,
                      ),
                      onPressed: () {
                        Navigator.push(
                            context,
                            new MaterialPageRoute(
                                builder: (context) => new CartPage()));
                      },
                    ),
                    new Positioned(
                      top: 0.0,
                      right: 4.0,
                      child: new Container(
                        height: 20.0,
                        width: 20.0,
                        decoration: new BoxDecoration(
                          shape: BoxShape.circle,
                          color: Colors.red,
                        ),
                        child: new Center(
                          child: container.cartItemList.isEmpty
                              ? new Text(
                                  "0",
                                  style: new TextStyle(
                                      color: Colors.white, fontSize: 12.0),
                                )
                              : new Text(
                                  "${container.cartItemList.length}",
                                  style: new TextStyle(
                                      color: Colors.white, fontSize: 12.0),
                                ),
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
            decoration: new BoxDecoration(
              gradient: new LinearGradient(
                  colors: [const Color(0xFF9E30BF), const Color(0xFF262BA0)],
                  begin: const FractionalOffset(0.0, 0.0),
                  end: const FractionalOffset(1.0, 0.0),
                  stops: [0.0, 1.0],
                  tileMode: TileMode.clamp),
            ),
          ),
          new Expanded(
            child: new CustomScrollView(
              slivers: <Widget>[
                new SliverPadding(
                  padding: new EdgeInsets.all(8.0),
                  sliver: new SliverToBoxAdapter(
                    child: new Text(
                      "Quick order",
                      style: Theme.of(context).textTheme.title,
                    ),
                  ),
                ),
                new SliverPadding(
                  padding: new EdgeInsets.all(8.0),
                  sliver: new SliverToBoxAdapter(
                    child: new GestureDetector(
                      onTap: () {
                        if (container.userAccessToken != null) {
                          Navigator.push(
                              context,
                              new MaterialPageRoute(
                                  builder: (context) =>
                                      new UploadPrescription()));
                        } else {
                          _showSnackBar("Please login first");
                        }
                      },
                      child: new Container(
                        height: 60.0,
                        decoration: new BoxDecoration(
                          gradient: new LinearGradient(
                              colors: [
                                const Color(0xFF9E30BF),
                                const Color(0xFF262BA0)
                              ],
                              begin: const FractionalOffset(0.0, 0.0),
                              end: const FractionalOffset(1.0, 0.0),
                              stops: [0.0, 1.0],
                              tileMode: TileMode.clamp),
                          borderRadius:
                              BorderRadius.all(new Radius.circular(5.0)),
                        ),
                        child: new Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            new Image(
                              image: new AssetImage("assets/prescription.png"),
                              height: 35.0,
                              width: 35.0,
                            ),
                            new Padding(
                              padding:
                                  new EdgeInsets.symmetric(horizontal: 16.0),
                            ),
                            new Text(
                              "Upload Prescription",
                              style: new TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.w600,
                                fontSize: 16.0,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
                new SliverPadding(
                  padding: new EdgeInsets.all(8.0),
                  sliver: new SliverToBoxAdapter(
                    child: new Text(
                      "Drugs, OTC & Wellness",
                      style: Theme.of(context).textTheme.title,
                    ),
                  ),
                ),
                new SliverPadding(
                  padding: new EdgeInsets.all(8.0),
                  sliver: new SliverGrid.count(
                    crossAxisCount: 3,
                    children: new List.generate(categoryList.length, (index) {
                      return new InkWell(
                        onTap: () {
                          Navigator.push(
                              context,
                              new MaterialPageRoute(
                                  builder: (context) => new CategoryPage(
                                        id: categoryList[index].id,
                                        title: categoryList[index].name,
                                      )));
                        },
                        child: new Card(
                          elevation: 5.0,
                          child: new Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              categoryList[index].image != null
                                  ? new Image.network(
                                      AppText.prefixImage +
                                          categoryList[index].image,
                                      height: 65.0,
                                      width: 65.0,
                                    )
                                  : new Image.asset(
                                      "assets/medication.png",
                                      height: 65.0,
                                      width: 65.0,
                                    ),
                              new Padding(
                                padding: new EdgeInsets.all(8.0),
                                child: new Text(
                                  categoryList[index].name.toUpperCase(),
                                  textAlign: TextAlign.center,
                                  style: new TextStyle(
                                    fontSize: 10.0,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      );
                    }),
                  ),
                ),
                new SliverPadding(
                  padding: new EdgeInsets.all(8.0),
                  sliver: new SliverToBoxAdapter(
                    child: new Text(
                      "Trending",
                      style: Theme.of(context).textTheme.title,
                    ),
                  ),
                ),
                new SliverPadding(
                  padding: new EdgeInsets.all(8.0),
                  sliver: new SliverToBoxAdapter(
                    child: new Container(
                      height: 150.0,
                      child: new ListView.builder(
                        scrollDirection: Axis.horizontal,
                        padding: new EdgeInsets.all(0.0),
                        itemBuilder: (context, index) {
                          return new InkWell(
                            onTap: () {
                              Navigator.push(
                                context,
                                new MaterialPageRoute(
                                  builder: (context) =>
                                      new MedicineDetailEndPage(
                                        onlineMedicineItem: trendingList[index],
                                      ),
                                  fullscreenDialog: true,
                                ),
                              );
                            },
                            child: new Card(
                              elevation: 5.0,
                              child: new Container(
                                width: 180.0,
                                padding: new EdgeInsets.all(8.0),
                                child: new Column(
                                  children: <Widget>[
                                    new Hero(
                                      tag: trendingList[index].id,
                                      child: new Container(
                                        child: trendingList[index].image != null
                                            ? new Image.network(
                                                AppText.prefixImage +
                                                    trendingList[index].image,
                                                height: 75.0,
                                                width: 75.0,
                                              )
                                            : new Image.asset(
                                                "assets/medication.png",
                                                height: 75.0,
                                                width: 75.0,
                                              ),
                                      ),
                                    ),
                                    new Padding(
                                      padding: new EdgeInsets.only(top: 4.0),
                                    ),
                                    new Text(
                                      trendingList[index].name,
                                      maxLines: 1,
                                      textAlign: TextAlign.center,
                                      overflow: TextOverflow.ellipsis,
                                      style: new TextStyle(
                                        fontWeight: FontWeight.w600,
                                      ),
                                    ),
                                    new Padding(
                                      padding: new EdgeInsets.only(top: 8.0),
                                    ),
                                    trendingList[index].rate ==
                                            trendingList[index].applicableRate
                                        ? new Align(
                                            alignment: Alignment.centerRight,
                                            child: new Text(
                                              "Rs. " +
                                                  trendingList[index]
                                                      .applicableRate,
                                              style: new TextStyle(
                                                fontWeight: FontWeight.bold,
                                              ),
                                            ),
                                          )
                                        : new Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceEvenly,
                                            children: <Widget>[
                                              new Text(
                                                "Rs. " +
                                                    trendingList[index].rate,
                                                style: new TextStyle(
                                                  fontWeight: FontWeight.bold,
                                                  color: Colors.red,
                                                  decoration: TextDecoration
                                                      .lineThrough,
                                                ),
                                              ),
                                              new Text(
                                                "Rs. " +
                                                    trendingList[index]
                                                        .applicableRate,
                                                style: new TextStyle(
                                                  fontWeight: FontWeight.bold,
                                                ),
                                              ),
                                            ],
                                          ),
                                    // new Align(
                                    //   alignment: Alignment.centerRight,
                                    //   child: new Text(
                                    //     "Rs. " +
                                    //         trendingList[index].applicableRate,
                                    //     style: new TextStyle(
                                    //       fontWeight: FontWeight.bold,
                                    //     ),
                                    //   ),
                                    // ),
                                    // new Padding(
                                    //   padding: new EdgeInsets.only(top: 8.0),
                                    // ),
                                    // new Align(
                                    //   alignment: Alignment.centerRight,
                                    //   child: new Text(
                                    //     "Rs. " +
                                    //         trendingList[index].applicableRate,
                                    //     style: new TextStyle(
                                    //       fontWeight: FontWeight.bold,
                                    //     ),
                                    //   ),
                                    // ),
                                  ],
                                ),
                              ),
                            ),
                          );
                        },
                        itemCount: trendingList.length,
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
