import 'package:flutter/material.dart';
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';
import 'package:online_medicine/appText.dart';
import 'package:online_medicine/appbar/gradientWithCart.dart';
import 'package:online_medicine/model/onlineMedicineItem.dart';
import 'package:online_medicine/stateContainer/stateContainer.dart';

class MedicineDetailEndPage extends StatefulWidget {
  final OnlineMedicineItem onlineMedicineItem;
  MedicineDetailEndPage({Key key, @required this.onlineMedicineItem})
      : super(key: key);
  @override
  _MedicineDetailEndPageState createState() => _MedicineDetailEndPageState();
}

class _MedicineDetailEndPageState extends State<MedicineDetailEndPage> {
  OnlineMedicineItem onlineMedicineItem;
  @override
  void initState() {
    onlineMedicineItem = widget.onlineMedicineItem;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final container = StateContainer.of(context);
    List<OnlineMedicineItem> cartList = container.cartItemList;
    List<OnlineMedicineItem> medicineList = container.medicineList;
    List<OnlineMedicineItem> substituteList = container.substituteList;
    List<String> compositionList = new List();

    addItemToCart() {
      onlineMedicineItem.quantityInCart = 1;
      for (int j = 0; j < medicineList.length; j++) {
        if (onlineMedicineItem.id == medicineList[j].id) {
          medicineList.removeAt(j);
          medicineList.insert(j, onlineMedicineItem);
        }
      }
      for (int j = 0; j < substituteList.length; j++) {
        if (onlineMedicineItem.id == substituteList[j].id) {
          substituteList.removeAt(j);
          substituteList.insert(j, onlineMedicineItem);
        }
      }
      container.updateSubstituteList(substituteList, false);
      container.updateMedicineList(medicineList, false);
      cartList.add(onlineMedicineItem);
      container.updateCartList(cartList);
    }

    updateItemInCart(bool method) {
      int quantity = onlineMedicineItem.quantityInCart;
      if (method == true) {
        onlineMedicineItem.quantityInCart = quantity + 1;
        for (int j = 0; j < medicineList.length; j++) {
          if (onlineMedicineItem.id == medicineList[j].id) {
            medicineList.removeAt(j);
            medicineList.insert(j, onlineMedicineItem);
          }
        }
        for (int j = 0; j < substituteList.length; j++) {
          if (onlineMedicineItem.id == substituteList[j].id) {
            substituteList.removeAt(j);
            substituteList.insert(j, onlineMedicineItem);
          }
        }
        container.updateSubstituteList(substituteList, false);
        container.updateMedicineList(medicineList, false);
        cartList.forEach((item) {
          if (item.id == onlineMedicineItem.id) {
            cartList.remove(item);
            cartList.add(onlineMedicineItem);
          }
        });
        container.updateCartList(cartList);
      } else {
        if (quantity == 1) {
          onlineMedicineItem.quantityInCart = 0;
          for (int j = 0; j < medicineList.length; j++) {
            if (onlineMedicineItem.id == medicineList[j].id) {
              medicineList.removeAt(j);
              medicineList.insert(j, onlineMedicineItem);
            }
          }
          for (int j = 0; j < substituteList.length; j++) {
            if (onlineMedicineItem.id == substituteList[j].id) {
              substituteList.removeAt(j);
              substituteList.insert(j, onlineMedicineItem);
            }
          }
          container.updateSubstituteList(substituteList, false);
          container.updateMedicineList(medicineList, false);
          cartList.removeWhere((item) => item.id == onlineMedicineItem.id);
          container.updateCartList(cartList);
        } else {
          onlineMedicineItem.quantityInCart = quantity - 1;
          for (int j = 0; j < medicineList.length; j++) {
            if (onlineMedicineItem.id == medicineList[j].id) {
              medicineList.removeAt(j);
              medicineList.insert(j, onlineMedicineItem);
            }
          }
          for (int j = 0; j < substituteList.length; j++) {
            if (onlineMedicineItem.id == substituteList[j].id) {
              substituteList.removeAt(j);
              substituteList.insert(j, onlineMedicineItem);
            }
          }
          container.updateSubstituteList(substituteList, false);
          container.updateMedicineList(medicineList, false);
          cartList.forEach((item) {
            if (item.id == onlineMedicineItem.id) {
              cartList.remove(item);
              cartList.add(onlineMedicineItem);
            }
          });
          container.updateCartList(cartList);
        }
      }
    }

    if (onlineMedicineItem.compositions != null) {
      compositionList = onlineMedicineItem.compositions.split(",");
    }

    return new Scaffold(
      body: new Column(
        children: <Widget>[
          new GradientAppBarWithCart(onlineMedicineItem.name, cartList, true),
          new Expanded(
            child: new SingleChildScrollView(
              padding: new EdgeInsets.all(8.0),
              child: new Container(
                padding: new EdgeInsets.all(8.0),
                decoration: new BoxDecoration(
                  border: new Border.all(color: Colors.grey, width: 0.7),
                ),
                child: new Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    new Hero(
                      tag: onlineMedicineItem.id,
                      child: new Container(
                        height: 200.0,
                        child: new Center(
                          child: onlineMedicineItem.image != null
                              ? new Image.network(
                                  AppText.prefixImage +
                                      onlineMedicineItem.image,
                                  height: 150.0,
                                  width: 150.0,
                                )
                              : new Image.asset(
                                  "assets/medication.png",
                                  height: 150.0,
                                  width: 150.0,
                                ),
                        ),
                      ),
                    ),
                    new Divider(
                      color: Colors.grey,
                      height: 20.0,
                    ),
                    new Text(
                      onlineMedicineItem.name,
                      style: new TextStyle(
                        fontSize: 16.0,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                    new Padding(
                      padding: new EdgeInsets.only(top: 8.0),
                    ),
                    new Text(
                      onlineMedicineItem.company,
                      style: new TextStyle(),
                    ),
                    new Padding(
                      padding: new EdgeInsets.only(top: 8.0),
                    ),
                    new Text(
                      "Compositions: ",
                      style: new TextStyle(fontWeight: FontWeight.bold),
                    ),
                    new Padding(
                      padding: new EdgeInsets.only(top: 8.0),
                    ),
                    new Column(
                      children: compositionList.map((item) {
                        return new Container(
                          padding: EdgeInsets.all(5.0),
                          child: Row(
                            children: <Widget>[
                              new Container(
                                height: 8.0,
                                width: 8.0,
                                margin:
                                    new EdgeInsets.symmetric(horizontal: 8.0),
                                decoration: new BoxDecoration(
                                  color: Colors.black54,
                                  shape: BoxShape.circle,
                                ),
                              ),
                              new Text(item),
                            ],
                          ),
                        );
                      }).toList(),
                    ),
                    new Padding(
                      padding: new EdgeInsets.only(top: 16.0),
                    ),
                    new Row(
                      children: <Widget>[
                        new Text(
                          "Rs. " + onlineMedicineItem.rate,
                          style: new TextStyle(
                            fontWeight: FontWeight.bold,
                            color: Colors.red,
                            decoration: TextDecoration.lineThrough,
                          ),
                        ),
                        new Padding(
                          padding: new EdgeInsets.only(left: 16.0),
                        ),
                        new Text(
                          "Rs. " + onlineMedicineItem.applicableRate,
                          style: new TextStyle(
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ],
                    ),
                    new Align(
                        alignment: Alignment.centerRight,
                        child: onlineMedicineItem.quantityInCart == 0
                            ? new InkWell(
                                splashColor: Colors.black,
                                onTap: () {
                                  addItemToCart();
                                },
                                child: new Container(
                                    height: 35.0,
                                    width: 110.0,
                                    decoration: new BoxDecoration(
                                      gradient: new LinearGradient(
                                          colors: [
                                            const Color(0xF29E30BF),
                                            const Color(0xF2262BA0)
                                          ],
                                          begin:
                                              const FractionalOffset(0.0, 0.0),
                                          end: const FractionalOffset(1.0, 0.0),
                                          stops: [0.0, 1.0],
                                          tileMode: TileMode.clamp),
                                    ),
                                    child: new Center(
                                      child: new Text(
                                        "ADD TO CART",
                                        style: new TextStyle(
                                          color: Colors.white,
                                          fontWeight: FontWeight.w600,
                                        ),
                                      ),
                                    )))
                            : new Container(
                                width: 110.0,
                                child: new Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    new GestureDetector(
                                      onTap: () {
                                        updateItemInCart(false);
                                      },
                                      child: new Container(
                                        padding: new EdgeInsets.all(3.0),
                                        decoration: new BoxDecoration(
                                          shape: BoxShape.circle,
                                          gradient: new LinearGradient(
                                              colors: [
                                                const Color(0xF29E30BF),
                                                const Color(0xF2262BA0)
                                              ],
                                              begin: const FractionalOffset(
                                                  0.0, 0.0),
                                              end: const FractionalOffset(
                                                  1.0, 0.0),
                                              stops: [0.0, 1.0],
                                              tileMode: TileMode.clamp),
                                        ),
                                        child: new Icon(
                                          Icons.remove,
                                          color: Colors.white,
                                          size: 20.0,
                                        ),
                                      ),
                                    ),
                                    new Container(
                                      padding: new EdgeInsets.only(
                                        left: 12.0,
                                        right: 12.0,
                                        top: 5.0,
                                        bottom: 5.0,
                                      ),
                                      decoration: new BoxDecoration(
                                          border: new Border.all(
                                        color: const Color(0xF29E30BF),
                                      )),
                                      child: new Text(
                                        onlineMedicineItem.quantityInCart
                                            .toString(),
                                        style: new TextStyle(
                                          fontSize: 14.0,
                                        ),
                                      ),
                                    ),
                                    new GestureDetector(
                                      onTap: () {
                                        updateItemInCart(true);
                                      },
                                      child: new Container(
                                        padding: new EdgeInsets.all(3.0),
                                        decoration: new BoxDecoration(
                                          shape: BoxShape.circle,
                                          gradient: new LinearGradient(
                                              colors: [
                                                const Color(0xF29E30BF),
                                                const Color(0xF2262BA0)
                                              ],
                                              begin: const FractionalOffset(
                                                  0.0, 0.0),
                                              end: const FractionalOffset(
                                                  1.0, 0.0),
                                              stops: [0.0, 1.0],
                                              tileMode: TileMode.clamp),
                                        ),
                                        child: new Icon(
                                          Icons.add,
                                          color: Colors.white,
                                          size: 20.0,
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              )),
                    onlineMedicineItem.description != null
                        ? new Divider(
                            color: Colors.grey,
                            height: 20.0,
                          )
                        : new Container(),
                    new Padding(
                      padding: new EdgeInsets.only(top: 8.0),
                    ),
                    onlineMedicineItem.description != null
                        ? new Text(
                            "Product Detail",
                            style: new TextStyle(
                              fontSize: 16.0,
                              fontWeight: FontWeight.bold,
                            ),
                          )
                        : new Container(),
                    new Padding(
                      padding: new EdgeInsets.only(top: 8.0),
                    ),
                    onlineMedicineItem.description != null
                        ? new Align(
                            alignment: Alignment.centerRight,
                            child: new InkWell(
                              splashColor: Colors.black,
                              onTap: () {
                                print(onlineMedicineItem.description);
                                Navigator.of(context).push(
                                    new MaterialPageRoute(
                                        builder: (context) =>
                                            new WebviewScaffold(
                                              url: new Uri.dataFromString(
                                                      onlineMedicineItem
                                                          .description,
                                                      mimeType: 'text/html')
                                                  .toString(),
                                              appBar: new AppBar(
                                                backgroundColor:
                                                    Color(0xFF9E30BF),
                                                title: new Text(
                                                  "Product Detail",
                                                ),
                                              ),
                                            ),
                                        fullscreenDialog: true));
                              },
                              child: new Container(
                                height: 35.0,
                                width: 110.0,
                                decoration: new BoxDecoration(
                                  gradient: new LinearGradient(
                                      colors: [
                                        const Color(0xF29E30BF),
                                        const Color(0xF2262BA0)
                                      ],
                                      begin: const FractionalOffset(0.0, 0.0),
                                      end: const FractionalOffset(1.0, 0.0),
                                      stops: [0.0, 1.0],
                                      tileMode: TileMode.clamp),
                                ),
                                child: new Center(
                                  child: new Text(
                                    "View Detail",
                                    style: new TextStyle(
                                      color: Colors.white,
                                      fontWeight: FontWeight.w600,
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          )
                        : new Container(),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
