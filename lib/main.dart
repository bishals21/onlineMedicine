import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:online_medicine/onlineMedicinePage.dart';
import 'package:online_medicine/stateContainer/stateContainer.dart';

void main() {
  SystemChrome.setPreferredOrientations(
      [DeviceOrientation.portraitUp, DeviceOrientation.portraitDown]);
  runApp(new MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      debugShowCheckedModeBanner: false,
      builder: (context, child) {
        return new ScrollConfiguration(
          behavior: MyBehavior(),
          child: new StateContainer(
            child: child,
          ),
        );
      },
      home: new OnlineMedicinePage(),
      routes: <String, WidgetBuilder>{
        '/homePage': (BuildContext context) => new OnlineMedicinePage(),
      },
    );
  }
}

class MyBehavior extends ScrollBehavior {
  @override
  Widget buildViewportChrome(
      BuildContext context, Widget child, AxisDirection axisDirection) {
    return child;
  }
}
