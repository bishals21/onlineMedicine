import 'package:flutter/material.dart';
import 'package:online_medicine/cartPage.dart';
import 'package:online_medicine/model/onlineMedicineItem.dart';

class GradientAppBarWithCart extends StatelessWidget {
  final String title;
  final List<OnlineMedicineItem> cartList;
  final double barHeight = 66.0;
  final bool isFullScreen;

  GradientAppBarWithCart(this.title, this.cartList, this.isFullScreen);

  @override
  Widget build(BuildContext context) {
    final double statusBarHeight = MediaQuery.of(context).padding.top;

    return new Column(
      children: <Widget>[
        new Container(
          //margin: new EdgeInsets.only(left: 10.0),
          padding: new EdgeInsets.only(top: statusBarHeight),
          height: statusBarHeight + barHeight,
          child: new Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              isFullScreen
                  ? new IconButton(
                      icon: new Icon(Icons.close),
                      color: Colors.white,
                      onPressed: () => Navigator.maybePop(context),
                    )
                  : new BackButton(color: Colors.white),
              new Expanded(
                child: new Text(
                  title,
                  overflow: TextOverflow.ellipsis,
                  style: const TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.w600,
                      fontSize: 18.0),
                ),
              ),
              new Stack(
                children: <Widget>[
                  new IconButton(
                    icon: new Icon(
                      Icons.shopping_cart,
                      color: Colors.white,
                      size: 24.0,
                    ),
                    onPressed: () {
                      Navigator.push(
                          context,
                          new MaterialPageRoute(
                              builder: (context) => new CartPage()));
                    },
                  ),
                  new Positioned(
                    top: 0.0,
                    right: 4.0,
                    child: new Container(
                      height: 20.0,
                      width: 20.0,
                      decoration: new BoxDecoration(
                        shape: BoxShape.circle,
                        color: Colors.red,
                      ),
                      child: new Center(
                        child: cartList.isEmpty
                            ? new Text(
                                "0",
                                style: new TextStyle(
                                    color: Colors.white, fontSize: 12.0),
                              )
                            : new Text(
                                "${cartList.length}",
                                style: new TextStyle(
                                    color: Colors.white, fontSize: 12.0),
                              ),
                      ),
                    ),
                  ),
                ],
              ),
            ],
          ),
          decoration: new BoxDecoration(
            gradient: new LinearGradient(
                colors: [const Color(0xFF9E30BF), const Color(0xFF262BA0)],
                begin: const FractionalOffset(0.0, 0.0),
                end: const FractionalOffset(1.0, 0.0),
                stops: [0.0, 1.0],
                tileMode: TileMode.clamp),
          ),
        ),
      ],
    );
  }
}
