import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:flutter_facebook_login/flutter_facebook_login.dart';
import 'package:http/http.dart' as http;
import 'package:online_medicine/appText.dart';
import 'package:online_medicine/model/userAccessToken.dart';
import 'package:online_medicine/stateContainer/stateContainer.dart';
import 'package:shared_preferences/shared_preferences.dart';

GoogleSignIn _googleSignIn = new GoogleSignIn(
  scopes: <String>[
    'email',
    'https://www.googleapis.com/auth/contacts.readonly',
  ],
);

class SocialLoginData {
  String token = "";
  String userId = "";
  String email = "";
  String name = "";
  String type = "";
}

class LoginData {
  String username = "";
  String password = "";
}

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final GlobalKey<FormState> _formKey = new GlobalKey<FormState>();
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  Future<SharedPreferences> _prefs = SharedPreferences.getInstance();

  static final FacebookLogin facebookSignIn = new FacebookLogin();

  LoginData loginData = new LoginData();
  bool _obscureText = true;

  String _validateUsername(String value) {
    if (value.length == 0) {
      return "Please enter username";
    }

    return null;
  }

  String _validatePassword(String value) {
    if (value.length == 0) {
      return "Please enter password";
    }

    return null;
  }

  Future<bool> checkConnection() async {
    bool connected = false;
    try {
      final result = await InternetAddress.lookup('google.com');
      if (result.isNotEmpty && result[0].rawAddress.isNotEmpty) {
        print('connected');
        connected = true;
      }
    } on SocketException catch (_) {
      print('not connected');
      connected = false;
    }

    return connected;
  }

  void _showSnackBar(String text) {
    _scaffoldKey.currentState.showSnackBar(new SnackBar(
      content: new Text(text),
    ));
  }

  ThemeData buildTheme() {
    final ThemeData base = ThemeData();
    return base.copyWith(
        hintColor: Colors.white,
        primaryColor: Colors.white,
        errorColor: Colors.white,
        splashColor: Colors.white24,
        inputDecorationTheme: new InputDecorationTheme(
            labelStyle: TextStyle(
          color: Colors.white,
        )));
  }

  @override
  Widget build(BuildContext context) {
    final double statusBarHeight = MediaQuery.of(context).padding.top;
    final container = StateContainer.of(context);

    _saveUser(UserAccessToken userAccessToken) async {
      final SharedPreferences prefs = await _prefs;
      bool isSuccess =
          await prefs.setString('OMUser', json.encode(userAccessToken));
      setState(() {
        if (isSuccess) {
          Navigator.pop(context);
          container.updateUserAccessToken(userAccessToken);
          print("User saved SuccessFully");
        }
      });
    }

    postLogin() async {
      bool isConnected = await checkConnection();
      if (isConnected) {
        http.post(AppText.baseURl + "auth/custom", body: {
          "username": "${loginData.username}",
          "password": "${loginData.password}",
        }, headers: {
          "api-key": AppText.apiKey
        }).then((response) {
          print("Response Body:${response.body}");

          var responseBody = json.decode(response.body);
          if (responseBody['status'] == true) {
            _showSnackBar(responseBody["message"]);
            var data = responseBody["data"];
            _saveUser(new UserAccessToken(
              data["accessToken"],
              data["name"],
              data["phone"],
              data["email"],
              data["address"],
              data["dob"],
            ));
            _showSnackBar(responseBody["message"]);
          } else {
            _showSnackBar(responseBody["message"]);
          }
        });
      } else {
        _showSnackBar("No Internet Connection");
      }
    }

    postLoginFromFHN(String username, String email, String token, String phone,
        String name) async {
      bool isConnected = await checkConnection();
      if (isConnected) {
        http.post(AppText.baseURl + "auth/fhn", body: {
          "username": username,
          "email": email,
          "token": token,
          "phone": phone,
          "name": name,
        }, headers: {
          "api-key": AppText.apiKey
        }).then((response) {
          print("Response Body:${response.body}");

          var responseBody = json.decode(response.body);
          if (responseBody['status'] == true) {
            _showSnackBar(responseBody["message"]);
            var data = responseBody["data"];
            _saveUser(new UserAccessToken(
              data["accessToken"],
              data["name"],
              data["phone"],
              data["email"],
              data["address"],
              data["dob"],
            ));
            _showSnackBar(responseBody["message"]);
          } else {
            _showSnackBar(responseBody["message"]);
          }
        });
      } else {
        _showSnackBar("No Internet Connection");
      }
    }

    postFHNLogin(FHNItem item) async {
      bool isConnected = await checkConnection();
      if (isConnected) {
        http.post("http://familyhealthnepal.com/api/login", body: {
          "username": "${item.fhnId}",
          "password": "${item.password}",
        }, headers: {
          "api-key": "testapikey"
        }).then((response) {
          print("Response Body:${response.body}");
          if (response.statusCode == 200) {
            var responseBody = json.decode(response.body);
            if (responseBody['status_code'] == 1000) {
              var data = responseBody["data"];
              // saveUser(new UserAccessToken(
              //     accessToken: data["accessToken"],
              //     fullName: data["fullname"],
              //     fhnId: data["fhnId"],
              //     image: data["image"]));\
              postLoginFromFHN(
                  data["fhnId"], "", data["accessToken"], "", data["fullname"]);
            } else {
              _showSnackBar(
                  responseBody["message"] + " " + responseBody["description"]);
            }
          }
        });
      } else {
        _showSnackBar("No Internet Connection");
      }
    }

    postFacebookLogin(
        String token, String userId, String email, String name) async {
      bool isConnected = await checkConnection();
      if (isConnected) {
        http.post(AppText.baseURl + "auth/social", body: {
          "token": token,
          "userId": userId,
          "email": email,
          "name": name,
          "type": "FB",
        }, headers: {
          "api-key": AppText.apiKey
        }).then((response) {
          print("Response Body:${response.body}");
          if (response.statusCode == 200) {
            var responseBody = json.decode(response.body);
            if (responseBody['status'] == true) {
              var data = responseBody["data"];
              _saveUser(new UserAccessToken(
                data["accessToken"],
                data["name"],
                data["phone"],
                data["email"],
                data["address"],
                data["dob"],
              ));
              _showSnackBar(responseBody["message"]);
            } else {
              _showSnackBar(responseBody["message"]);
            }
          }
        });
      } else {
        _showSnackBar("No Internet Connection");
      }
    }

    Future<Null> _fbLogin() async {
      final FacebookLoginResult result =
          await facebookSignIn.logInWithReadPermissions(['email']);

      switch (result.status) {
        case FacebookLoginStatus.loggedIn:
          final FacebookAccessToken accessToken = result.accessToken;
          var graphResponse = await http.get(
              'https://graph.facebook.com/v2.12/me?fields=name,first_name,last_name,email&access_token=${accessToken.token}');
          var profile = json.decode(graphResponse.body);
          print('''
         Logged in!
         
         Token: ${accessToken.token}
         User id: ${accessToken.userId}
         Expires: ${accessToken.expires}
         Permissions: ${accessToken.permissions}
         Email: ${profile['email']}
         ''');

          postFacebookLogin(accessToken.token, accessToken.userId,
              profile['email'], profile['name']);
          break;
        case FacebookLoginStatus.cancelledByUser:
          print('Login cancelled by the user.');
          break;
        case FacebookLoginStatus.error:
          print('Something went wrong with the login process.\n'
              'Here\'s the error Facebook gave us: ${result.errorMessage}');
          break;
      }
    }

    postGoogleLogin(GoogleSignInAccount account) async {
      bool isConnected = await checkConnection();
      if (isConnected) {
        http.post(AppText.baseURl + "auth/social", body: {
          "userId": "${account.id}",
          "email": "${account.email}",
          "name": "${account.displayName}",
          "type": "GOOGLE",
        }, headers: {
          "api-key": AppText.apiKey
        }).then((response) {
          print("Response Body:${response.body}");
          if (response.statusCode == 200) {
            var responseBody = json.decode(response.body);
            if (responseBody['status'] == true) {
              var data = responseBody["data"];
              _saveUser(new UserAccessToken(
                data["accessToken"],
                data["name"],
                data["phone"],
                data["email"],
                data["address"],
                data["dob"],
              ));
              _showSnackBar(responseBody["message"]);
            } else {
              _showSnackBar(responseBody["message"]);
            }
          }
        });
      } else {
        _showSnackBar("No Internet Connection");
      }
    }

    void submit() {
      if (this._formKey.currentState.validate()) {
        _formKey.currentState.save();
        postLogin();
      }
    }

    Future<Null> _googleLogin() async {
      try {
        await _googleSignIn.signIn();
        print("signed in" + _googleSignIn.currentUser.displayName);
        postGoogleLogin(_googleSignIn.currentUser);
      } catch (error) {
        print(error);
      }
    }

    _navigateToFHNLogin() async {
      final FHNItem item = await showDialog(
          context: context,
          builder: (context) {
            return AlertDialog(
              content: new FhnLoginDialog(),
            );
          });

      if (item != null) {
        postFHNLogin(item);
      }
    }

    return new Theme(
      data: buildTheme(),
      child: new Scaffold(
        key: _scaffoldKey,
        body: Stack(
          children: <Widget>[
            new Container(
              decoration: new BoxDecoration(
                image: new DecorationImage(
                    image: AssetImage("assets/medicalImage.jpg"),
                    fit: BoxFit.cover),
              ),
              child: new Container(
                decoration: new BoxDecoration(
                  gradient: new LinearGradient(
                      colors: [
                        const Color(0xB39E30BF),
                        const Color(0xB3262BA0)
                      ],
                      begin: const FractionalOffset(0.0, 0.0),
                      end: const FractionalOffset(1.0, 0.0),
                      stops: [0.0, 1.0],
                      tileMode: TileMode.clamp),
                ),
                child: new Container(
                  margin: new EdgeInsets.only(top: 70.0),
                  child: new Form(
                    key: _formKey,
                    child: new ListView(
                      padding: new EdgeInsets.symmetric(
                        horizontal: 8.0,
                        vertical: 8.0,
                      ),
                      children: <Widget>[
                        // new Padding(
                        //   padding: new EdgeInsets.all(16.0),
                        //   child: new Text(
                        //     "",
                        //     textAlign: TextAlign.center,
                        //     style: new TextStyle(
                        //       color: Colors.white,
                        //       fontWeight: FontWeight.bold,
                        //       fontSize: 22.0,
                        //     ),
                        //   ),
                        // ),
                        new Padding(
                          padding: new EdgeInsets.only(top: 8.0),
                          child: new TextFormField(
                            keyboardType: TextInputType.text,
                            style: new TextStyle(
                              color: Colors.white,
                              fontSize: 16.0,
                            ),
                            decoration: new InputDecoration(
                              hintText: "Enter your username",
                              labelText: "Username",
                              hintStyle: new TextStyle(color: Colors.white),
                              border: new OutlineInputBorder(),
                            ),
                            validator: this._validateUsername,
                            onSaved: (String value) {
                              this.loginData.username = value;
                            },
                          ),
                        ),
                        new Padding(
                          padding: new EdgeInsets.only(top: 24.0),
                          child: new TextFormField(
                            keyboardType: TextInputType.text,
                            obscureText: _obscureText,
                            style: new TextStyle(
                              color: Colors.white,
                              fontSize: 16.0,
                            ),
                            decoration: new InputDecoration(
                                hintText: "Enter your password",
                                labelText: "Password",
                                hintStyle: new TextStyle(color: Colors.white),
                                border: new OutlineInputBorder(),
                                suffixIcon: new GestureDetector(
                                  child: new Icon(
                                    _obscureText
                                        ? Icons.visibility_off
                                        : Icons.visibility,
                                    color: Colors.white,
                                  ),
                                  onTap: () {
                                    setState(() {
                                      _obscureText = !_obscureText;
                                    });
                                  },
                                )),
                            validator: this._validatePassword,
                            onSaved: (String value) {
                              this.loginData.password = value;
                            },
                          ),
                        ),
                        // new Padding(
                        //   padding: new EdgeInsets.only(top: 24.0),
                        //   child: new ListTile(
                        //     title: new Text(
                        //       AppLocalization.of(context).signingText,
                        //       style: new TextStyle(
                        //         color: Colors.white,
                        //       ),
                        //     ),
                        //     onTap: () {
                        //       showModalBottomSheet(
                        //           context: context,
                        //           builder: (context) => SingleChildScrollView(
                        //                 child: Column(
                        //                   children: <Widget>[
                        //                     Padding(
                        //                       padding:
                        //                           const EdgeInsets.all(8.0),
                        //                       child: new Text(
                        //                         "Terms and Conditions",
                        //                         style: new TextStyle(
                        //                           color: Colors.black,
                        //                           fontSize: 22.0,
                        //                         ),
                        //                       ),
                        //                     ),
                        //                     new Container(
                        //                       child: new HtmlView(
                        //                         data: msg,
                        //                       ),
                        //                     ),
                        //                   ],
                        //                 ),
                        //               ));
                        //     },
                        //   ),
                        // ),
                        new Padding(
                            padding: new EdgeInsets.only(top: 24.0),
                            child: new MaterialButton(
                              height: 50.0,
                              onPressed: submit,
                              color: Color(0x80FFFFFF),
                              child: new Text(
                                "LOGIN",
                                style: new TextStyle(
                                  color: Colors.white,
                                  fontSize: 16.0,
                                ),
                              ),
                            )),
                        new Padding(
                          padding: new EdgeInsets.only(top: 24.0, bottom: 8.0),
                          child: new Divider(
                            color: Colors.white,
                            height: 10.0,
                          ),
                        ),
                        new Padding(
                            padding: new EdgeInsets.only(top: 24.0),
                            child: new MaterialButton(
                                height: 50.0,
                                textColor: Colors.white,
                                onPressed: () {
                                  _navigateToFHNLogin();
                                },
                                color: Colors.white,
                                child: new Row(
                                  children: <Widget>[
                                    new Image.asset(
                                      "assets/fhnLogo.png",
                                      height: 30.0,
                                      width: 30.0,
                                    ),
                                    new Expanded(
                                      child: new Text(
                                        "LOGIN WITH FHN ID",
                                        textAlign: TextAlign.center,
                                        style: new TextStyle(
                                          color: Colors.black,
                                          fontSize: 16.0,
                                        ),
                                      ),
                                    ),
                                  ],
                                ))),
                        new Padding(
                            padding: new EdgeInsets.only(top: 24.0),
                            child: new MaterialButton(
                                height: 50.0,
                                textColor: Colors.white,
                                onPressed: _fbLogin,
                                color: Color(0xFF3B5998),
                                child: new Row(
                                  children: <Widget>[
                                    new Image.asset(
                                      "assets/facebookLogo.png",
                                      height: 30.0,
                                      width: 30.0,
                                    ),
                                    new Expanded(
                                      child: new Text(
                                        "LOGIN WITH FACEBOOK",
                                        textAlign: TextAlign.center,
                                        style: new TextStyle(
                                          color: Colors.white,
                                          fontSize: 16.0,
                                        ),
                                      ),
                                    ),
                                  ],
                                ))),
                        new Padding(
                            padding: new EdgeInsets.only(top: 24.0),
                            child: new MaterialButton(
                                height: 50.0,
                                textColor: Colors.white,
                                onPressed: _googleLogin,
                                color: Color(0xFFFFFFFF),
                                child: new Row(
                                  children: <Widget>[
                                    new Image.asset(
                                      "assets/googlePlusLogo.png",
                                      height: 30.0,
                                      width: 30.0,
                                    ),
                                    new Expanded(
                                      child: new Text(
                                        "LOGIN WITH GOOGLE PLUS",
                                        textAlign: TextAlign.center,
                                        style: new TextStyle(
                                          color: Colors.black,
                                          fontSize: 16.0,
                                        ),
                                      ),
                                    ),
                                  ],
                                ))),
                      ],
                    ),
                  ),
                ),
              ),
            ),
            new Padding(
              padding: new EdgeInsets.only(top: statusBarHeight),
              child: new Row(
                children: <Widget>[
                  new IconButton(
                    color: Colors.white,
                    icon: new Icon(Icons.close),
                    onPressed: () => Navigator.maybePop(context),
                  ),
                  new Container(
                    padding: const EdgeInsets.symmetric(horizontal: 25.0),
                    child: new Text(
                      "LOGIN",
                      style: const TextStyle(
                          color: Colors.white,
                          fontWeight: FontWeight.w600,
                          fontSize: 20.0),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class FhnLoginDialog extends StatefulWidget {
  @override
  _FhnLoginDialogState createState() => _FhnLoginDialogState();
}

class _FhnLoginDialogState extends State<FhnLoginDialog> {
  final GlobalKey<FormState> _formKey = new GlobalKey<FormState>();
  String fhnId = "";
  String password = "";

  String _validateId(String value) {
    if (value.length == 0) {
      return "FHN ID is required";
    }
    return null;
  }

  String _validatePassword(String value) {
    if (value.length == 0) {
      return "Password is required";
    }
    return null;
  }

  ThemeData buildTheme() {
    final ThemeData base = ThemeData();
    return base.copyWith(
        hintColor: Color(0xFF9E30BF),
        primaryColor: Color(0xFF9E30BF),
        accentColor: Color(0xFF9E30BF),
        inputDecorationTheme: new InputDecorationTheme(
            labelStyle: TextStyle(
          color: Color(0xFF9E30BF),
        )));
  }

  @override
  Widget build(BuildContext context) {
    void sendValue() {
      if (this._formKey.currentState.validate()) {
        _formKey.currentState.save();
        Navigator.pop(context, new FHNItem(fhnId: fhnId, password: password));
      }
    }

    return new Theme(
      data: buildTheme(),
      child: Container(
        child: new Form(
          key: _formKey,
          child: new Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              new TextFormField(
                keyboardType: TextInputType.text,
                initialValue: fhnId,
                decoration: new InputDecoration(
                  hintText: "Enter FHN ID",
                  labelText: "FHN ID",
                  contentPadding: new EdgeInsets.symmetric(
                      horizontal: 16.0, vertical: 16.0),
                  hintStyle: new TextStyle(color: Colors.grey),
                  border: new OutlineInputBorder(),
                ),
                validator: this._validateId,
                onSaved: (String value) {
                  fhnId = value;
                },
              ),
              new Padding(
                padding: new EdgeInsets.only(top: 16.0),
              ),
              new TextFormField(
                keyboardType: TextInputType.text,
                initialValue: fhnId,
                obscureText: true,
                decoration: new InputDecoration(
                  hintText: "Enter password",
                  labelText: "Password",
                  contentPadding: new EdgeInsets.symmetric(
                      horizontal: 16.0, vertical: 16.0),
                  hintStyle: new TextStyle(color: Colors.grey),
                  border: new OutlineInputBorder(),
                ),
                validator: this._validatePassword,
                onSaved: (String value) {
                  password = value;
                },
              ),
              new Row(
                mainAxisAlignment: MainAxisAlignment.end,
                children: <Widget>[
                  new FlatButton(
                    child: new Text(
                      "Cancel",
                      style: new TextStyle(color: Colors.red),
                    ),
                    onPressed: () {
                      Navigator.pop(context);
                    },
                  ),
                  new FlatButton(
                    child: new Text(
                      "Login",
                      style: new TextStyle(color: Color(0xFF9E30BF)),
                    ),
                    onPressed: sendValue,
                  )
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}

class FHNItem {
  FHNItem({@required this.fhnId, @required this.password});
  String fhnId;
  String password;
}
